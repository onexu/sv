`ifndef TEST_SV
`define TEST_SV

module automatic test;
	task main();
		fork
			$display("@%0t: Line %0d, subproc 1", $time, `__LINE__); // 第一个子进程
			#20 $display("@%0t: Line %0d, subproc 2", $time, `__LINE__); // 第二个子进程
		join_none
	endtask

	initial begin
		$display("@%0t: Line %0d, proc", $time, `__LINE__);
		main(); // 启动子进程
		wait fork; // 阻塞主进程
		#10 $display("@%0t: Line %0d, proc", $time, `__LINE__);
	end
endmodule

`endif
