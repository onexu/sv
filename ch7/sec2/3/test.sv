`ifndef TEST_SV
`define TEST_SV

module automatic test;
	bit [3:0] i;

	task transmit(input int n);
		process proc[] = new [n];
		foreach (proc[j]) begin
			fork
				automatic int k = j;
				begin
					proc[k] = process::self();
					$display("start subprocess%0d", k);
					wait (i == k);
				end
			join_none
		end
		#100; // 超时
		foreach (proc[j]) begin
			$display("subprocess%0d.status=%s", j, proc[j].status());
			if (proc[j].status != process::FINISHED) begin
				proc[j].kill();
				$display("subprocess%0d.status=%s", j, proc[j].status());
			end
		end
	endtask

	initial transmit(4);

	initial repeat(2) #10 i++;
endmodule

`endif
