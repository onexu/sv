`ifndef TEST_SV
`define TEST_SV

module automatic test;
	bit [3:0] i;

	task transmit(input transaction tr);
		fork
			begin
				fork: timeout
					begin // 第一个孙进程
						wait (i == tr.a);
						$display("@%0t: check %0d", $time, tr.a);
					end
					#40 $display("@%0t: check a=%0d timeout.", $time, tr.a); // 第二个孙进程
				join_any
				disable timeout; // disable fork;
			end
		join_none
	endtask

	initial begin
		transaction blueprint;
		blueprint = new();
		repeat (8) begin
			transaction tr;
			tr = new();
			assert(blueprint.randomize());
			tr.copy(blueprint);
			tr.print("generate");
			transmit(tr); // 启动8个子进程
		end
	end
	initial repeat(16) #10 i++;
endmodule

`endif
