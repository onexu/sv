`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer;
	event done;

	function new (input event done); // 从类外传入事件句柄
		this.done = done;
	endfunction

	task main();
		fork
			begin
				// 执行一些操作
				->> done; // 触发事件
			end
		join_none
	endtask
endclass

`endif
