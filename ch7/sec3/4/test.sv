`ifndef TEST_SV
`define TEST_SV

`include "sequencer.svh"

parameter N = 16;

module automatic test;
	initial begin
		event done[N];
		sequencer seqr[N];

		foreach (seqr[i]) begin
			seqr[i] = new(done[i]); // 创建sequencer对象
			seqr[i].main(); // 调用sequencer对象的main方法
		end

		foreach (seqr[i]) begin
			fork
				automatic int k = i;
				wait (done[k].triggered);
			join_none
		end

		wait fork; // 等待所有sequencer对象的done事件被触发
	end
endmodule

`endif
