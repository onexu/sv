`ifndef TEST_SV
`define TEST_SV

module automatic test;
	event e1, e2;
	process proc1, proc2;

	initial begin
		proc1 = process::self();
		$display("@%0t: 1: before trigger", $time);
		-> e1;
		wait(e2.triggered);
		$display("@%0t: 1: after trigger", $time);
	end

	initial begin
		proc2 = process::self();
		$display("@%0t: 2: before trigger", $time);
		-> e2;
		wait(e1.triggered);
		$display("@%0t: 2: after trigger", $time);
	end

	initial begin
		#100;
		$display("proc1.status=%s", proc1.status());
		$display("proc2.status=%s", proc2.status());
	end
endmodule

`endif
