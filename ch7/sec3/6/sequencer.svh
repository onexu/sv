`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer;
	static int proc_cnt = 0; // 静态属性

	task main();
		proc_cnt++; // 启动进程时proc_cnt加1
		fork
			proc_cnt--; // 结束进程时proc_cnt减1
		join_none
	endtask
endclass

`endif
