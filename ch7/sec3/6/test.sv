`ifndef TEST_SV
`define TEST_SV

`include "sequencer.svh"

parameter N = 16;

module automatic test;
	initial begin
		sequencer seqr[N]; // 创建sequencer对象
		foreach (seqr[i]) begin // 调用sequencer对象的main方法
			seqr[i] = new();
			seqr[i].main();
		end
		wait (sequencer::proc_cnt == 0); // 等待proc_cnt等于0
	end
endmodule

`endif
