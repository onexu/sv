`ifndef TEST_SV
`define TEST_SV

`include "sequencer.svh"

module automatic test;
	initial begin
		event done;
		sequencer seqr;
		seqr = new(done);
		seqr.main();
		wait(done.triggered); // 等待事件被触发
	end
endmodule

`endif
