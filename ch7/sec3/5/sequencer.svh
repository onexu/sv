`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer;
	event done;

	function new (input event done); // Pass event frome Testbench
		this.done = done;
	endfunction

	task main();
		fork
			begin
				// Create transaction
				-> done; // Tell the test we are done
			end
		join_none
	endtask
endclass

`endif
