`ifndef TEST_SV
`define TEST_SV

`include "sequencer.svh"

parameter N = 16;

module automatic test;
	initial begin
		int done_count;
		event done[N];
		sequencer seqr[N];

		foreach (seqr[i]) begin
			seqr[i] = new(done[i]); // 创建sequencer对象
			seqr[i].main(); // 调用sequencer对象的main方法
		end

		foreach (seqr[i]) begin // 有done事件被触发则计数器加1
			fork
				automatic int k = i;
				wait (done[k].triggered);
				done_count++;
			join_none
		end

		wait (done_count == N); // 等待计数器值等于N
	end
endmodule

`endif
