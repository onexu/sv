`ifndef TEST_SV
`define TEST_SV

module automatic test();
	semaphore sem; // 定义一个信号量
	initial begin
		sem = new(3); // 为信号量分配3个钥匙
		fork // 创建3个子进程
			proc1();
			proc2();
			proc3();
		join
	end

	task automatic proc1();
		sem.get(1); // 尝试获取1个钥匙
		#10 $display("Run proc1 at %0t.", $time);
		sem.put(1); // 返回钥匙
	endtask

	task automatic proc2();
		sem.get(2); // 尝试获取2个钥匙
		#10 $display("Run proc2 at %0t.", $time);
		sem.put(2);
	endtask

	task automatic proc3();
		sem.get(3); // 尝试获取3个钥匙
		#10 $display("Run proc3 at %0t.", $time);
		sem.put(3);
	endtask
endmodule

`endif
