`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver #(type T = transaction);
	mailbox #(T) seqr2drv;
	T tr;

	function new(input mailbox #(T) seqr2drv);
		this.seqr2drv = seqr2drv;
	endfunction

	virtual task main();
		forever begin
			seqr2drv.get(tr); // 使用信箱接收句柄，获得transaction对象
			tr.print("driver");
		end
	endtask
endclass

`endif