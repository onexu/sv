`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"
`include "sequencer.svh"
`include "driver.svh"

module automatic test();
	mailbox #(transaction) seqr2drv;
	transaction blueprint;
	sequencer seqr;
	driver drv;

	initial begin
		seqr2drv = new();
		blueprint = new();
		seqr = new(seqr2drv);
		drv = new(seqr2drv);
		seqr.blueprint = blueprint;
		seqr.num_trans = 4;
		fork
			seqr.main();
			drv.main();
		join
	end
endmodule

`endif
