`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"
`include "sequencer.svh"
`include "driver.svh"

module automatic test;
	mailbox #(transaction) seqr2drv;
	mailbox #(byte) handshake;
	transaction blueprint;
	sequencer seqr;
	driver drv;
	initial begin
		seqr2drv = new();
		handshake = new();
		blueprint = new();
		seqr = new(seqr2drv, handshake);
		drv = new(seqr2drv, handshake);
		seqr.num_trans = 4;
		seqr.blueprint = blueprint;
		fork
			seqr.main();
			drv.main();
		join;
	end
endmodule

`endif
