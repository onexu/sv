`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver #(type T = transaction);
	T tr;
	mailbox #(T) seqr2drv;
	mailbox #(byte) handshake;

	function new(input mailbox #(T) seqr2drv, input mailbox #(byte) handshake);
		this.seqr2drv = seqr2drv;
		this.handshake = handshake;
	endfunction

	virtual task main();
		forever begin
			seqr2drv.get(tr); // 使用信箱接收transaction
			tr.print("driver");
			handshake.put(1);
		end
	endtask
endclass

`endif
