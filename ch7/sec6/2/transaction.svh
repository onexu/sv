`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction #(parameter WIDTH = 4);
	rand bit [WIDTH-1:0] a;
	rand bit [WIDTH-1:0] b;

	virtual function void copy(input transaction rhs = null);
		if (rhs == null)
			$display("copy fail");
		a = rhs.a; // Copy data fields
		b = rhs.b;
	endfunction

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
