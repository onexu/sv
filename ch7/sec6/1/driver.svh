`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver #(type T = transaction);
	mailbox #(T) seqr2drv;
	T tr;
	event handshake;

	function new(input mailbox #(T) seqr2drv, input event handshake);
		this.seqr2drv = seqr2drv;
		this.handshake = handshake;
	endfunction

	virtual task main();
		forever begin
			seqr2drv.get(tr); // 使用信箱接收transaction
			tr.print("driver");
			->handshake;
		end
	endtask
endclass

`endif
