`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T blueprint;
	mailbox #(T) seqr2drv;
	event handshake;

	function new(input mailbox #(T) seqr2drv, input event handshake);
		this.seqr2drv = seqr2drv;
		this.handshake = handshake;
	endfunction

	virtual task main();
		repeat (num_trans) begin
			T tr;
			tr = new();
			assert(blueprint.randomize());
			tr.copy(blueprint);
			seqr2drv.put(tr); // 使用信箱发送transaction到driver
			tr.print("sequencer");
			@handshake;
		end
	endtask
endclass

`endif
