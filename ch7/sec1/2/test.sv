`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	bit [3:0] i;

	task transmit(input transaction tr);
		fork // 启动子进程
			begin
				wait (i == tr.a); // 阻塞子进程
				$write("@%0t,", $time);
				tr.print("transmit");
			end
		join_none
	endtask

	initial begin
		transaction blueprint;
		blueprint = new();
		repeat (6) begin
			transaction tr;
			tr = new();
			assert(blueprint.randomize());
			tr.copy(blueprint); // 保存transaction对象随机化后内容
			tr.print("generate");
			transmit(tr);
		end
	end

	initial repeat(16) #10 i++;
endmodule

`endif
