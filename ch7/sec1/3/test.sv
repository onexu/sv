`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		for (int i=0; i<3; i++) begin
			fork
				$display("i=%0d", i);
			join_none
		end
	end
endmodule

`endif
