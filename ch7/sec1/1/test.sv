`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		$display("@%0t: Line %0d, proc", $time, `__LINE__);
		#10 $display("@%0t: Line %0d, proc", $time, `__LINE__);
		fork
			$display("@%0t: Line %0d, subproc 1", $time, `__LINE__); // 第一个子进程
			#30 $display("@%0t: Line %0d, subproc 2", $time, `__LINE__); // 第二个子进程
			begin // 第三个子进程
				#20 $display("@%0t: Line %0d, subproc 3-1", $time, `__LINE__);
				#20 $display("@%0t: Line %0d, subproc 3-2", $time, `__LINE__);
			end
			#10 $display("@%0t: Line %0d, subproc 4", $time, `__LINE__); // 第四个子进程
		join // join_none或join_any
		$display("@%0t: Line %0d, proc", $time, `__LINE__);
		#10 $display("@%0t: Line %0d, proc", $time, `__LINE__);
		#50 $finish();
	end
endmodule

`endif
