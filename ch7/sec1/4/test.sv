`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		for (int i=0; i<3; i++) begin
			fork
				automatic int k = i;
				$display("k=%0d ", k);
			join_none
		end
	end
endmodule

`endif
