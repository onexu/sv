`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function int do_call_py();

module automatic test;
	initial begin
		if (do_call_py())
			$display("Call Python failed.");
		else
			$display("Call Python successed.");
	end
endmodule

`endif
