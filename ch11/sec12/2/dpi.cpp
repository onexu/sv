#ifndef DPI_C
#define DPI_C

#include <iostream>
#include <Python.h>
#include <svdpi.h>
#include <veriuser.h>
 
using namespace std;
 
int call_py()
{
	// 初始化python接口
	Py_Initialize();
	if(!Py_IsInitialized()){
		cout << "Python init fail" << endl;
		return 1;
	}

	// 设置python文件路径并读入
	PyRun_SimpleString("import sys");
	PyRun_SimpleString("sys.path.append('..')");
	PyObject* pModule = PyImport_ImportModule("hello");
	if(pModule == NULL)
	{
		cout <<"Module not found." << endl;
		return 1;
	}

	// 检索python函数
	PyObject* pFunc = PyObject_GetAttrString(pModule, "say_hello");
	if(!pFunc || !PyCallable_Check(pFunc))
	{
		cout <<"Function not found." << endl;
		return 1;
	}

	// 调用python函数
	PyObject_CallObject(pFunc, NULL);

	// 结束python接口和Python/C API函数的所有初始化
	Py_Finalize();
	return 0;
}

#ifdef __cplusplus
extern "C" {
#endif
	// 包装函数
	int do_call_py()
	{
		return call_py();
	}
#ifdef __cplusplus
}
#endif

#endif
