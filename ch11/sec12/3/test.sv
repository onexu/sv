`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function int do_call_py(input int a, input int b);

module automatic test;
	int a;
	int b;
	initial begin
		a = $urandom_range(0, 255);
		b = $urandom_range(0, 255);
		if (do_call_py(a, b))
			$display("Call Python failed.");
		else
			$display("Call Python successed.");
	end
endmodule

`endif
