`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void call_py();

module automatic test;
	initial call_py();
endmodule

`endif
