#ifndef DPI_C
#define DPI_C

#include <Python.h>
#include <svdpi.h>
#include <veriuser.h>

void call_py()
{
	// 初始化python接口
	Py_Initialize();
	// 执行python脚本命令
	PyRun_SimpleString("print('Hello world.')");
	// 结束python接口和Python/C API函数的所有初始化
	Py_Finalize();
}

#endif
