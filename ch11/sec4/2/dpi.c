#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

typedef struct { // 保存计数值的结构
	unsigned char c;
} cnt_s;

void* create_counter()
{
	cnt_s* counter = (cnt_s*) malloc(sizeof(cnt_s));
	counter->c = 0;
	return counter;
}

void do_count(
	cnt_s* counter,
	svBitVecVal* o,
	const svBitVecVal* i,
	const svBit reset,
	const svBit load)
{
	if (reset) counter->c = 0; // 复位
	else if (load) counter->c = *i; // 加载预设值
	else counter->c++; // 计数
	*o = counter->c & 0xf; // 屏蔽高位
	io_printf("C: o=%x, i=%x, reset=%x, load=%x\n", *o, *i, reset, load);
}

#endif
