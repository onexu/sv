`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function chandle create_counter();

import "DPI-C" function void do_count
	(input chandle cnt,
	output logic [3:0] out,
	input logic [3:0] in,
	input logic reset, load);

// 测试程序例化2个计数器
module automatic test;
	bit clk;
	logic [3:0] i0, i1, o0, o1;
	logic reset, load;
	chandle cnt0, cnt1; // 指向C中的存储空间

	initial begin
		forever #50 clk = ~clk;
	end

	initial begin
		reset = 0; // 初始化
		load = 0;
		i0 = 'h1;
		i1 = 'h7;
		@(negedge clk) reset <= 1; // 复位
		@(negedge clk) begin
			reset <= 0; // 取消复位
			load <= 1; // 加载预设值
			i0 <= 'hx;
			i1 <= 'hz;
		end
		@(negedge clk) load <= 0; // 计数
		@(negedge clk) $finish();
	end

	initial begin
		cnt0 = create_counter();
		cnt1 = create_counter();
		fork
			forever @(posedge clk) begin
				do_count(cnt0, o0, i0, reset, load);
				do_count(cnt1, o1, i1, reset, load);
			end
		join_none
	end
endmodule

`endif
