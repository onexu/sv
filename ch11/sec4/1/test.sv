`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void do_count(
	output bit [3:0] out,
	input bit [3:0] in,
	input bit reset, load);

module automatic test;
	bit [3:0] out, in;
	bit reset, load;

	initial begin
		reset = 1;
		load = 0;
		in = 4'he;
		do_count(out, in, reset, load); // 复位计数器
		#10 reset = 0;
		load = 1;
		do_count(out, in, reset, load); // 加载预设值
		#10 load = 0;
		repeat(4)
			#10 do_count(out, in, reset, load); // 计数
	end
endmodule

`endif
