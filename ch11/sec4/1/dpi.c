#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void do_count(
	svBitVecVal* o,
	const svBitVecVal* i,
	const svBit reset,
	const svBit load)
{
	static unsigned char counter = 0; // 静态计数器变量

	if (reset) counter = 0; // 复位
	else if (load) counter = *i; // 加载预设值
	else counter++; // 计数
	*o = counter & 0xf; // 屏蔽高4位
	io_printf("C: o=%x, i=%x, reset=%x, load=%x\n", *o, *i, reset, load);
}

#endif
