module top_tb;
	import "DPI-C" context function void c_display();
	export "DPI-C" function sv_display;

	test i_test();
	initial c_display();

	function void sv_display();
		$display("SV: top");
	endfunction
endmodule
