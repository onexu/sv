#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

extern void sv_display();
svScope scope;

void save_scope()
{
	scope = svGetScope();
}

void c_display()
{
	io_printf("C: default scope is %s\n", svGetNameFromScope(svGetScope()));
	sv_display();
	svSetScope(scope);
	io_printf("C: current scope is %s\n", svGetNameFromScope(svGetScope()));
	sv_display();
}

#endif
