module top_tb;
	export "DPI-C" function sv_display;
	import "DPI-C" context function void save_scope();

	test i_test();
	initial save_scope();

	function void sv_display();
		$display("SV: %m");
	endfunction
endmodule
