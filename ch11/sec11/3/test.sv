`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" context function void c_display();
	export "DPI-C" function sv_display;

	function void sv_display();
		$display("SV: %m");
	endfunction

	initial c_display();
endmodule

`endif
