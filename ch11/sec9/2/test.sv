`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function chandle create_counter();
import "DPI-C" function void do_count(input chandle inst);
import "DPI-C" function void do_load(input chandle inst, input bit [3:0] i);
import "DPI-C" function void do_reset(input chandle inst);
import "DPI-C" function byte unsigned do_get(input chandle inst);

// 使用counter类封装所有的导入函数和C++的对象句柄
class counter;
	chandle inst;
	
	function new();
		inst = create_counter();
	endfunction
	
	virtual function void count();
		do_count(inst);
	endfunction
	
	virtual function void load(bit [3:0] val);
		do_load(inst, val);
	endfunction
	
	virtual function void reset();
		do_reset(inst);
	endfunction
	
	virtual function bit [3:0] get();
		return do_get(inst);
	endfunction
endclass

module automatic test;
	initial begin
		counter inst;
		inst = new();

		inst.reset();
		$display("SV: reset: counter=%0h", inst.get());
		
		inst.load('he);
		$display("SV: load: counter=%0h", inst.get());

		repeat(4) begin
			inst.count();
			$display("SV: count: counter=%0h", inst.get());
		end
	end
endmodule

`endif
