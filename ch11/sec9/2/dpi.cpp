#ifndef DPI_CPP
#define DPI_CPP

#include <svdpi.h>

class counter{
public:
	counter();
	void do_count();
	void load(const svBitVecVal* i);
	void reset();
	unsigned char get();
private:
	unsigned char c;
};

counter::counter() // Initialize counter
{
	c = 0;
}

void counter::do_count() // Increment counter
{
	c++;
	c &= 0xf; // Mask upper bit
}

void counter::load(const svBitVecVal* i)
{
	c = *i;
	c &= 0xf; // Mask upper bit
}

void counter::reset()
{
	c = 0;
}

unsigned char counter::get()
{
	return c;
}

#ifdef __cplusplus
extern "C" {
#endif
	void* create_counter()
	{
		return new counter;
	}
	
	void do_count(void* inst)
	{
		((counter*)inst)->do_count();
	}
	
	void do_load(void* inst, const svBitVecVal* i)
	{
		((counter*)inst)->load(i);
	}
	
	void do_reset(void* inst)
	{
		((counter*)inst)->reset();
	}
	
	unsigned char do_get(void* inst)
	{
		return ((counter*)inst)->get();
	}
#ifdef __cplusplus
}
#endif

#endif
