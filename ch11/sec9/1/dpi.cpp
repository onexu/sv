#ifndef DPI_CPP
#define DPI_CPP

#include <svdpi.h>

class counter{
public:
	counter();
	void do_count();
	void load(const svBitVecVal* i);
	void reset();
	unsigned char get();
private:
	unsigned char c;
};

counter::counter() // 构造函数，计数器清零
{
	c = 0;
}

void counter::do_count() // 计数器加1
{
	c++;
	c &= 0xf; // 屏蔽高位
}

void counter::load(const svBitVecVal* i) // 加载预设值
{
	c = *i;
	c &= 0xf; // 屏蔽高位
}

void counter::reset() // 复位
{
	c = 0;
}

unsigned char counter::get() // 读计数器
{
	return c;
}

#ifdef __cplusplus
extern "C" {
#endif
	void* create_counter()
	{
		return new counter;
	}
	
	void do_count(void* inst)
	{
		((counter*)inst)->do_count();
	}
	
	void do_load(void* inst, const svBitVecVal* i)
	{
		((counter*)inst)->load(i);
	}
	
	void do_reset(void* inst)
	{
		((counter*)inst)->reset();
	}
	
	unsigned char do_get(void* inst)
	{
		return ((counter*)inst)->get();
	}
#ifdef __cplusplus
}
#endif

#endif
