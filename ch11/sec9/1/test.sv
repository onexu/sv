`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function chandle create_counter();
import "DPI-C" function void do_count(input chandle inst);
import "DPI-C" function void do_load(input chandle inst, input bit [3:0] i);
import "DPI-C" function void do_reset(input chandle inst);
import "DPI-C" function byte unsigned do_get(input chandle inst);

module automatic test;
	initial begin
		chandle inst;
		inst = create_counter();

		do_reset(inst);
		$display("SV: reset: counter=%0h", do_get(inst));

		do_load(inst, 'he);
		$display("SV: load: counter=%0h", do_get(inst));

		repeat(4) begin
			do_count(inst);
			$display("SV: count: counter=%0h", do_get(inst));
		end
	end
endmodule

`endif
