`ifndef TEST_SV
`define TEST_SV

typedef struct {
	bit [7:0] r;
	bit [7:0] g;
	bit [7:0] b;
} pixel_s;

import "DPI-C" function void invert(inout pixel_s pixel);

module automatic test;
	initial begin
		pixel_s pixel;
		pixel.r = $urandom_range(0, 255);
		pixel.g = $urandom_range(0, 255);
		pixel.b = $urandom_range(0, 255);
		$display("SV: pixel=%0p", pixel);
		invert(pixel); // 调用C层的invert
		$display("SV: pixel=%0p", pixel);
	end
endmodule

`endif
