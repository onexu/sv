#include<stdio.h>
 
int main()
{
	typedef struct
	{
		char r;
		char g;
		int  b;
	} color_s;

	color_s color = {1, 2, 3};
	printf("color's address=%p\n", &color);
	printf("color.r=%d, address=%p\n", color.r, &color.r);
	printf("color.g=%d, address=%p\n", color.g, &color.g);
	printf("color.b=%d, address=%p\n", color.b, &color.b);
} 
