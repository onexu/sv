#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

typedef struct {
	char unsigned r;
	char unsigned g;
	char unsigned b;
} *p_pixel_s;

void invert(p_pixel_s p_pixel)
{
	p_pixel->r = ~p_pixel->r; // 取反
	p_pixel->g = ~p_pixel->g;
	p_pixel->b = ~p_pixel->b;
	io_printf("C: pixel=%x, %x, %x\n", p_pixel->r, p_pixel->g, p_pixel->b);
}

#endif
