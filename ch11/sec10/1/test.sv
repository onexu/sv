`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" context function check();
	export "DPI-C" function print; // 没有类型和参数
	export "DPI-C" function push;
	export "DPI-C" function pop;

	int fifo[$]; // 保存数据值

	function void print();
		$display("SV: fifo=%0p", fifo);
	endfunction

	function void push(input int i); // 推入数据
		fifo.push_back(i);
	endfunction

	function int pop(); // 弹出数据
		return fifo.pop_front();
	endfunction

	initial check();
endmodule

`endif
