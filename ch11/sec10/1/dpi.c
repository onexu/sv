#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>
#include <stdio.h>

extern void print();
extern void push(int);
extern int pop();

void check()
{
	int data;
	io_printf("C: input data: ");
	scanf("%d", &data);
	push(data);
	io_printf("C: fifo push %d\n", data);
	print();
	data = pop();
	io_printf("C: fifo pop %d\n", data);
	print();
}

#endif
