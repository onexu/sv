#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>
#include <stdio.h>

extern void print();
extern void create();
extern void push(int, int);
extern void pop(int, int*);

void check()
{
	int data;
	create();
	create();
	io_printf("C: input data: ");
	scanf("%d", &data);
	push(0, data);
	io_printf("C: fifo0 write %d\n", data);
	push(1, data);
	io_printf("C: fifo1 write %d\n", data);
	pop(0, &data);
	io_printf("C: fifo0 read %d\n", data);
	print(0);
	print(1);
}

#endif
