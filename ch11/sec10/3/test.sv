`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" context task check();
	export "DPI-C" function create; // 没有类型和参数
	export "DPI-C" function print;
	export "DPI-C" task push;
	export "DPI-C" task pop;

	class fifo;
		int buffer[$]; // 保存数据值

		function void print();
			$display("SV: fifo=%p", buffer);
		endfunction

		task push(input int i); // 插入到队列最后
			#10 buffer.push_back(i);
		endtask

		task pop(output int data); // 弹出队列首个元素
			#10 data = buffer.pop_front();
		endtask
	endclass

	fifo fifoq[$]; // 句柄队列
	
	// 创建一个fifo对象并追加到队列中
	function void create();
		fifo f;
		f = new();
		fifoq.push_back(f);
	endfunction

	function void print(input int idx);
		fifoq[idx].print();
	endfunction

	task push(input int idx, input int data);
		fifoq[idx].push(data);
	endtask

	task pop(input int idx, output int data);
		fifoq[idx].pop(data);
	endtask

	initial check();
endmodule

`endif

