#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>
#include <stdio.h>

extern void print();
extern void push(int);
extern void pop(int*);

void check()
{
	int data;
	io_printf("C: input data: ");
	scanf("%d", &data);
	push(data);
	io_printf("C: fifo push %d\n", data);
	print();
	pop(&data);
	io_printf("C: fifo pop %d\n", data);
	print();
}

#endif
