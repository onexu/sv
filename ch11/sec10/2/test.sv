`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" context task check();
	export "DPI-C" function print; // 没有类型和参数
	export "DPI-C" task push;
	export "DPI-C" task pop;

	int fifo[$]; // 保存数据值

	function void print();
		$display("SV: fifo=%0p", fifo);
	endfunction

	task push(input int i); // 插入到队列最后
		#10 fifo.push_back(i);
	endtask

	task pop(output int data); // 弹出队列首个元素
		#10 data = fifo.pop_front();
	endtask

	initial check();
endmodule

`endif
