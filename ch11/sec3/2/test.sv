`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" \byte = function void cbyte();
	import "DPI-C" fabs = function real cfabs(input real r);

	initial begin
		cbyte();
		$display("cfabs(-1.0)=%f", cfabs(-1.0));
	end
endmodule

`endif
