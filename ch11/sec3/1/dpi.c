#ifndef DPI_C
#define DPI_C

#include<svdpi.h>

int factorial(const int i)
{
	if (i<=1) return 1;
	else return i*factorial(i-1);
}

#endif
