`ifndef TEST_SV
`define TEST_SV

module automatic test;
	import "DPI-C" function real fabs(input real r);
	import "DPI-C" function int factorial(input int r);

	initial begin
		$display("fabs(-1.0)=%f", fabs(-1.0));
		for(int i=0;i<10;i++)
			$display("factorial(%0d)=%0d", i, factorial(i));
	end
endmodule

`endif
