`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void print_scalar(input bit a, input logic b);

module automatic test;
	bit a;
	logic b;

	initial begin
		a = 1'b0;
		b = 1'bz;
		print_scalar(a, b);
		a = 1'b1;
		b = 1'bx;
		print_scalar(a, b);
	end
endmodule

`endif
