#ifndef DPI_C
#define DPI_C

#include "svdpi.h"
#include "veriuser.h"

void print_scalar(svBit a, svLogic b)
{
	io_printf("a=%x, b=%x\n", a, b);
}

#endif
