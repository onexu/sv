`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void print_logic_vec(input logic [39:0] a);

module automatic test;
	logic [39:0] a;

	initial begin
		a=40'h1234_5678_zx;
		$display("SV: data sent to C side: %x", a);
		print_logic_vec(a);
	end
endmodule

`endif
