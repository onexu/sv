#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void print_logic_vec(const svLogicVecVal* a)
{
	io_printf("C data from SV side\n");
	for(int i = 0; i < SV_PACKED_DATA_NELEMS(40); i++)
		io_printf("a[%x].aval=%x, a[%x].bval=%x\n", i, a[i].aval, i, a[i].bval);
}

#endif
