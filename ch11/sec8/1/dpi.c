#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

typedef struct {
	int r;
	int g;
	int b;
} *p_pixel_s;

void invert(p_pixel_s p_pixel)
{
	p_pixel->r = (~p_pixel->r) & 0xff; // 取反
	p_pixel->g = (~p_pixel->g) & 0xff;
	p_pixel->b = (~p_pixel->b) & 0xff;
	io_printf("C: pixel=%x, %x, %x\n", p_pixel->r, p_pixel->g, p_pixel->b);
}

char* print(p_pixel_s p_pixel)
{
	char *s; // char s[32]; 编译出错
	s = (char*) malloc(32*sizeof(char));
	sprintf(s, "C: pixel=%x, %x, %x", p_pixel->r, p_pixel->g, p_pixel->b);
	return s;
}

#endif
