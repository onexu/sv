#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void getElem(const svOpenArrayHandle h)
{
	io_printf("C: %p\n", svGetArrayPtr(h));
	io_printf("C: %p\n", svGetArrElemPtr(h, 0, 0));
	io_printf("C: %p\n", svGetArrElemPtr(h, 1, 3));
	io_printf("C: %d\n", *(int *)svGetArrElemPtr(h, 1, 3));
	io_printf("C: %d\n", *(int *)svGetArrElemPtr2(h, 0, 0));
}

#endif
