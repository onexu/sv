`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void getElem(inout int h[][]);

module automatic test;
	int a[2][4]; // a[1:0][3:0]会有不同的结果
	initial begin
		foreach (a[i,j]) a[i][j] = i+j;
		$display("SV: a=%0p", a);
		getElem(a);
	end
endmodule

`endif
