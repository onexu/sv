#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void print(const svOpenArrayHandle h)
{
	for(int i=svLow(h, 1); i<=svHigh(h, 1); i++)
	{
		for(int j=svLow(h, 2); j<=svHigh(h, 2); j++)
		{
			int *a = (int*) svGetArrElemPtr2(h, i, j);
			io_printf("C: a[%d][%d]=%d\n", i, j, *a);
			*a = i - j;
		}
	}
}

#endif
