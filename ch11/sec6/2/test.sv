`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void print(inout int h[][]);

module automatic test;
	int a[][];
	initial begin
		a = new[2];
		foreach (a[i]) a[i] = new[4];
		foreach (a[i,j]) a[i][j] = i+j;
		print(a);
		$display("SV: a=%0p", a);
	end
endmodule

`endif
