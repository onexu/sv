`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void print_pack(input bit [31:0] oa[]);

module automatic test;
	bit [0:3][7:0] a[4];
	initial begin
		foreach(a[i]) a[i] = i;
		a[1] = 64'h12345678;
		$display("SV: a=%0p", a);
		$display("SV: a[1]=%0h", a[1]);
		$display("SV: a[1][0]=%0h", a[1][0]);
		print_pack(a);
	end
endmodule

`endif
