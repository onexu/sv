#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void print_pack(const svOpenArrayHandle oa)
{
	for (int i = svLow(oa,1); i <= svHigh(oa,1); i++)
		io_printf("C: oa[%d]=%x\n", i, *(int *)svGetArrElemPtr1(oa, i));
}

#endif
