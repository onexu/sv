`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void fib_oa(inout bit [31:0] data[]);

module automatic test;
	bit [31:0] data[];

	initial begin
		data = new[10];
		fib_oa(data);
		foreach (data[i])
            $display("fib_oa(%0d)=%0d", i, data[i]);
	end
endmodule

`endif
