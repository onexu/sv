#ifndef DPI_C
#define DPI_C

#include <svdpi.h>
#include <veriuser.h>

void inquire(const svOpenArrayHandle h)
{
	io_printf("C: %d, %d\n", svLeft(h, 0), svRight(h, 0));
	io_printf("C: %d, %d\n", svLeft(h, 1), svRight(h, 1));
	io_printf("C: %d, %d\n", svLeft(h, 2), svRight(h, 2));
	io_printf("C: %d, %d\n", svLow(h, 0), svHigh(h, 0));
	io_printf("C: %d, %d\n", svLow(h, 1), svHigh(h, 1));
	io_printf("C: %d, %d\n", svLow(h, 2), svHigh(h, 2));
	io_printf("C: %d, %d, %d\n", svIncrement(h, 0), svIncrement(h, 1), svIncrement(h, 2));
	io_printf("C: %d, %d, %d\n", svSize(h, 0), svSize(h, 1), svSize(h, 2));
	io_printf("C: %d\n", svDimensions(h));
	io_printf("C: %d\n", svSizeOfArray(h));
}

#endif
