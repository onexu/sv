#ifndef DPI_C
#define DPI_C

#include <svdpi.h>

void fib(svLogicVecVal data[10])
{
	int i;
	data[0].aval = 1; // 同时写入aval和bval
	data[0].bval = 0;
	data[1].aval = 1;
	data[1].bval = 0;
	for (i=2; i<10; i++)
	{
		data[i].aval = data[i-1].aval + data[i-2].aval;
		data[i].bval = 0;
	}
}

#endif
