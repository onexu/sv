`ifndef TEST_SV
`define TEST_SV

import "DPI-C" function void fib(output logic [31:0] data[10]);

module automatic test;
	logic [31:0] data[10];
	initial begin
		fib(data);
		foreach (data[i]) $display("fib(%0d)=%0d", i, data[i]);
	end
endmodule

`endif
