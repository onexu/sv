#ifndef DPI_C
#define DPI_C

#include <svdpi.h>

void fib(svBitVecVal* data)
{
	data[0] = 1;
	data[1] = 1;
	for (int i=2; i<10; i++)
		data[i] = data[i-1] + data[i-2];
}

#endif
