`ifndef GUARD_STACK_INT_SVH
`define GUARD_STACK_INT_SVH

class stack_int;
	local int stack[$]; // 保存数据值

	virtual function void push(input int i); // 栈顶压入值
		stack.push_back(i);
	endfunction

	virtual function int pop(); // 栈顶弹出值
		return stack.pop_back();
	endfunction
endclass

`endif
