`ifndef TEST_SV
`define TEST_SV

`include "stack_int.svh"

module automatic test;
	const int size = 8;
	initial begin
		stack_int stacki;
		stacki = new(); // Construct a stack of int
		for(int i = 0; i < size; i++)
			stacki.push(i); // Push values onto stack
		repeat(size)
			$display("%0d", stacki.pop()); // Pop values off stack
	end
endmodule

`endif
