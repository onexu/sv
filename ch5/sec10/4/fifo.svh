`ifndef GUARD_FIFO_SVH
`define GUARD_FIFO_SVH

virtual class fifo_base;
	pure virtual function bit is_empty(); // 检查fifo是否为空
	pure virtual function bit is_full(); // 检查fifo是否已满
endclass

class fifo #(int DEPTH = 16, type T = int) extends fifo_base;
	T queue[DEPTH]; // 存储fifo数据的数组
	int front;
	int rear;
	int count;

	function new();
		front = 0;
		rear = 0;
		count = 0;
	endfunction

	virtual function void write(T data); // 向fifo中写入数据
		if (count < DEPTH) begin
			queue[rear] = data;
			rear = (rear + 1) % DEPTH;
			count++;
		end
		else begin
			$display("fifo is full");
		end
	endfunction

	virtual function T read(); // 从fifo中读取数据
		T data;
		if (count > 0) begin
			data = queue[front];
			front = (front + 1) % DEPTH;
			count--;
		end
		else begin
			$display("fifo is empty");
			data = '0; // 返回默认值
		end
		return data;
	endfunction

	virtual function bit is_empty(); // 检查fifo是否为空
		return (count == 0);
	endfunction

	virtual function bit is_full(); // 检查fifo是否已满
		return (count == DEPTH);
	endfunction
endclass

`endif
