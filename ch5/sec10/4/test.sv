`ifndef TEST_SV
`define TEST_SV

`include "fifo.svh"

module automatic test;
	initial begin
		fifo_base fifo_q[$];
		fifo #(16, bit[3:0]) fifo0 = new();
		fifo #(16, int) fifo1 = new();
		fifo #(16, byte) fifo2 = new();
		
		fifo_q = {fifo0, fifo1, fifo2};

		for(int i = 0; i < 16; i++) begin // 写FIFO
			fifo0.write(i);
			fifo1.write(i);
			fifo2.write(i);
			$display("write %0d", i);
		end

		foreach(fifo_q[i])
			$display("fifo full: %0d", fifo_q[i].is_full());
	end
endmodule

`endif
