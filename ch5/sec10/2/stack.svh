`ifndef GUARD_STACK_SVH
`define GUARD_STACK_SVH

class stack #(type T = int);
	local T stack[$]; // 保存数据值

	virtual function void push(input T i); // 栈顶压入值
		stack.push_back(i);
	endfunction

	virtual function T pop(); // 栈顶弹出值
		return stack.pop_back();
	endfunction
endclass

`endif
