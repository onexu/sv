`ifndef TEST_SV
`define TEST_SV

`include "stack.svh"

module automatic test;
	const int size = 8;
	initial begin
		stack #(real) stacker;
		stacker = new(); // 创建一个real类型的栈
		for(int i = 0; i < size; i++)
			stacker.push(i * 1.0); // 将数值压入栈
		repeat(size)
			$display("%f", stacker.pop()); // 将数值弹出栈
	end
endmodule

`endif
