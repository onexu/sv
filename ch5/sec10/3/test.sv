`ifndef GUARD_TEST_SV
`define GUARD_TEST_SV

`include "fifo.svh"

module automatic test;
	initial begin
		fifo #(16, int) ufifo = new();
		do begin // 写FIFO
			int t = $urandom_range(0, 32'hff);
			ufifo.write(t);
			$display("write %0d", t);
		end while (!ufifo.is_full());

		do begin // 读FIFO
			int t = ufifo.read();
			$display("read %0d", t);
		end while (!ufifo.is_empty());
	end
endmodule

`endif
