class bus_item extends object;
	bit [31:0] addr; // 地址
	bit [31:0] data; // 数据
	bit write; // 0读1写

	extern function new(string name = "");
	extern virtual function void print();
endclass

function bus_item::new(string name);
	super.new(name);
endfunction

function void bus_item::print();
	super.print();
	$display("addr=%0h", addr);
	$display("data=%0h", data);
	$display("write=%0h", write);
endfunction
