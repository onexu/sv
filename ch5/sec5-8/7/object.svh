virtual class object;
	local string name;
	protected static int m_inst_count;
	
	extern function new(string name = "");
	extern virtual function void set_name(string name);
	extern virtual function string get_name();
	extern static function int get_inst_count();
	pure virtual function void print();
endclass

function object::new(string name);
	this.name = name;
	m_inst_count++;
endfunction

function void object::set_name(string name);
	this.name = name;
endfunction

function string object::get_name();
	return name;
endfunction

function int object::get_inst_count();
	return m_inst_count;
endfunction
