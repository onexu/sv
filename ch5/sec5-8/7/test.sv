`include "object.svh"
`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item;
		item = new("item");
		item.print();
		item.set_name("new_item");
		$display("%s", item.get_name());
		$display("%0d", bus_item::get_inst_count());
	end
endmodule
