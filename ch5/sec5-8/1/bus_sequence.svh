typedef class object;
typedef class bus_item;

class bus_sequence extends object;
	bus_item write_item[];
	bus_item read_item;
	
	extern function new(string name = "");
	extern function void body();
	extern function void print();
endclass

function bus_sequence::new(string name);
	super.new(name);
endfunction

function void bus_sequence::body();
	write_item = new[4];
	foreach(write_item[i]) begin
		write_item[i] = new($sformatf("write_item%0d", i));
		write_item[i].write = 1;
	end
	read_item = new("read_item");
	read_item.write = 0;
endfunction

function void bus_sequence::print();
	super.print();
	foreach(write_item[i]) begin
		write_item[i].print();
	end
	read_item.print();
endfunction
