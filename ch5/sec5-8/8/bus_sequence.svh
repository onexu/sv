typedef class object;
typedef class bus_item;

class bus_sequence extends object;
	bus_item read_item;
	
	extern function new(string name = "");
	extern virtual function void body(); // 虚方法
	extern virtual function void print(); // 虚方法
endclass

function bus_sequence::new(string name);
	super.new(name);
endfunction

function void bus_sequence::body();
	read_item = new("read_item");
	read_item.write = 0;
endfunction

function void bus_sequence::print();
	super.print();
	read_item.print();
endfunction
