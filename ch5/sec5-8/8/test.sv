`include "object.svh"
`include "bus_item.svh"
`include "bus_sequence.svh"

module automatic test;
	initial begin
		bus_sequence seq0, seq1;
		seq0 = new("seq0"); // 创建原始对象
		seq0.body();
		foreach(seq0.write_item[i]) begin
			seq0.write_item[i].addr = i + 'h10;
			seq0.write_item[i].data = i;
		end
		seq1 = new seq0; // 使用new操作符
		foreach(seq0.write_item[i]) begin
			seq0.write_item[i].addr = 0;
			seq0.write_item[i].data = 0;
		end
		seq1.print(); // 打印副本对象内容
	end
endmodule
