`include "object.svh"
`include "bus_item.svh"
`include "bus_sequence.svh"

module automatic test;
	initial begin
		object obj;
		bus_item item;
		bus_sequence seq;
		item = new("item"); // 创建派生类对象
		seq = new("seq"); // 创建派生类对象
		seq.body();
		obj = item; // 基类句柄引用派生类对象
		obj.print();
		obj = seq; // 基类句柄引用派生类对象
		obj.print();
	end
endmodule
