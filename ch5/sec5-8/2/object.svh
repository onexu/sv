class object;
	string name;
	
	extern function new(string name = "");
	extern function void set_name(string name);
	extern function string get_name();
	extern virtual function void print(); // 虚方法
endclass

function object::new(string name);
	this.name = name;
endfunction

function void object::set_name(string name);
	this.name = name;
endfunction

function string object::get_name();
	return name;
endfunction

function void object::print();
endfunction
