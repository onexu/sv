`include "object.svh"
`include "bus_item.svh"

module automatic test;
	initial begin
		object obj;
		bus_item item;
		item = new("item"); // 创建派生类对象
		obj = item; // 基类句柄引用派生类对象
		if(!$cast(item, obj)) // 尝试向下类型转换
			$display("casting fail");
		else
			item.print();
	end
endmodule
