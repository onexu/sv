typedef class object;

class bus_item extends object;
	bit [31:0] addr; // 地址
	bit [31:0] data; // 数据
	bit write; // 0读1写

	extern function new(string name = "");
	extern virtual function void copy(object rhs = null);
	extern virtual function void print(); // 虚方法
endclass

function bus_item::new(string name);
	super.new(name);
endfunction

function void bus_item::copy(object rhs);
	super.copy(rhs);
	begin
		bus_item rhs_; // 派生类句柄
		if ((rhs == null) || !$cast(rhs_, rhs)) begin // 句柄向下类型转换
			$display("cast fail");
			return;
		end
		addr = rhs_.addr; // 复制属性
		data = rhs_.data;
		write = rhs_.write;
	end
endfunction

function void bus_item::print();
	super.print();
	$display("addr=%0h", addr);
	$display("data=%0h", data);
	$display("write=%0h", write);
endfunction
