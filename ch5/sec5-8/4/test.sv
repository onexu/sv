`include "object.svh"
`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item;
		$display("amount=%0h", bus_item::get_inst_count);
		item = new("item");
		$display("amount=%0h", item.get_inst_count);
		item.print();
	end
endmodule
