`include "object.svh"
`include "addr_range.svh"
`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item;
		item.range.start_addr = 0;
		item.range.end_addr = 'h1f;
		// item = new("item");
		item.range.print();
	end
endmodule
