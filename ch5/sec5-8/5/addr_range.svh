class addr_range extends object;
	bit [31:0] start_addr;
	bit [31:0] end_addr;

	extern function new(string name = "");
	extern function void print();
endclass

function addr_range::new(string name);
	super.new(name);
endfunction

function void addr_range::print();
	$display("start_addr=%0h", start_addr);
	$display("end_addr=%0h", end_addr);
endfunction
