`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class config_info;
	int unsigned amount;
endclass

class transaction;
	bit [3:0] a;
	static config_info cfg_info = new(); // 静态句柄，启动前调用new完成例化

	function new(input bit [3:0] a = 0);
		this.a = a;
		cfg_info.amount++;
	endfunction

	virtual function void print(string name = "");
		$display("%s: a=%0h", name, a);
	endfunction

	static function void print_cfg_info();
		$display("%0p", cfg_info);
	endfunction
endclass

`endif
