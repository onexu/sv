`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		transaction::print_cfg_info();
		tr = new();
		tr.print_cfg_info();
	end
endmodule

`endif
