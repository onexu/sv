`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item; // 定义对象句柄
		item = new(); // 创建一个bus_item对象
		item.addr = 8;
		item.data = 5;
		item.write = 1;
		$display("item=%0p", item);
		item.print();
	end
endmodule
