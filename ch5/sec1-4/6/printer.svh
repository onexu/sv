class printer;
	extern function void print_int(string name, int value);
	extern function void print_string(string name, string value);
endclass

function void printer::print_int(string name, int value);
	$display("%s=%0d", name, value);
endfunction

function void printer::print_string(string name, string value);
	$display("%s=\"%s\"", name, value);
endfunction
