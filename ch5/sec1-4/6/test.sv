`include "printer.svh"
`include "bus_item.svh"

printer default_printer = new();

module automatic test;
	initial begin
		bus_item item;
		item = new("item");
		item.addr = 8;
		item.data = 5;
		item.write = 1;
		item.print();
	end
endmodule
