class bus_item;
	string name; // 对象名
	bit [31:0] addr; // 地址
	bit [31:0] data; // 数据
	bit write; // 0读1写

	extern function new(string name = "");
	extern function void set_name(string name);
	extern function string get_name();
	extern function void print();
endclass

function bus_item::new(string name);
	this.name = name;
endfunction

function void bus_item::set_name(string name);
	this.name = name;
endfunction

function string bus_item::get_name();
	return name;
endfunction

function void bus_item::print();
	$display("addr=%0h", addr);
	$display("data=%0h", data);
	$display("write=%0h", write);
endfunction
