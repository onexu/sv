`include "bus_item.svh"

function automatic void pass_value(bus_item item);
	item = new("item1"); // 不影响实参句柄
endfunction

function automatic void pass_ref(ref bus_item item);
	item = new("item1"); // 影响实参句柄
endfunction

module automatic test;
	initial begin
		bus_item item;
		item = new("item0");
		pass_value(item);
		$display("%s", item.get_name());
		pass_ref(item);
		$display("%s", item.get_name());
	end
endmodule
