`include "bus_item.svh"
`include "bus_sequence.svh"

module automatic test;
	initial begin
		bus_sequence seq;
		seq = new("seq");
		seq.body();
		seq.print();
	end
endmodule
