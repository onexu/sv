`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item; // 定义句柄
		$display("type=%s, width=%0d", $typename(item), $bits(item)); // 打印句柄的类型名和位宽
		$display("ID=%0h", item);// 打印对象ID
		$display("object=%0p", item); // 打印对象内容
	end
endmodule
