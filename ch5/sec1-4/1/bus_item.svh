class bus_item;
	bit [31:0] addr; // 地址
	bit [31:0] data; // 数据
	bit write; // 0读1写
	
	function void print();
		$display("addr=%0h", addr);
		$display("data=%0h", data);
		$display("write=%0h", write);
	endfunction
endclass
