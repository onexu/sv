`include "bus_item.svh"

module automatic test;
	initial begin
		bus_item item0, item1;
		item0 = new("item0");
		item1 = new("item1");
		item0 = item1;
		item1 = null;
		item0.print();
	end
endmodule
