class monitor implements subject; // 具体主题类
	string name;
	int state;
	observer observers[$];

	function new(string name);
		this.name = name;
	endfunction

	virtual function void attach(observer o);
		observers.push_back(o);
	endfunction

	virtual function void detach(observer o);
		foreach (observers[idx]) begin
			if (observers[idx] == o) begin
				observers.delete(idx);
				break;
			end
		end
	endfunction

	virtual function void notify();
		foreach (observers[idx]) begin
			observers[idx].update(state);
		end
	endfunction

	virtual function void set_state(int state);
		this.state = state;
		notify();
	endfunction

	virtual function int get_state();
		return state;
	endfunction
endclass
