interface class subject; // 抽象主题类
	pure virtual function void attach(observer o); // 注册观察者
	pure virtual function void detach(observer o); // 删除观察者
	pure virtual function void notify(); // 通知所有已注册观察者
endclass
