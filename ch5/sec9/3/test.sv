`include "observer.svh"
`include "subject.svh"
`include "scoreboard.svh"
`include "monitor.svh"

module test();
	initial begin
		monitor mon = new("mon");
		scoreboard scb0 = new("scb0");
		scoreboard scb1 = new("scb1");

		mon.attach(scb0);
		mon.attach(scb1);

		$display("setting mon's state to 10");
		mon.set_state(10);

		mon.detach(scb1);
		$display("setting mon's state to 20");
		mon.set_state(20);
	end
endmodule
