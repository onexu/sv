class scoreboard implements observer; // 具体观察者类
	string name;
	int state;

	function new(string name);
		this.name = name;
	endfunction

	virtual function void update(int state);
		this.state = state;
		$display("%s's state updated to %0d", name, state);
	endfunction
endclass
