class insurer;
	vehicle_insure vi;

	function new(vehicle_insure vi);
		this.vi = vi;
	endfunction

	virtual function int unsigned insure(); // 计算保险费用
		int engine_size = vi.get_engine_size();
		int num_accidents = vi.get_num_accidents();
		$display("The insurance is %0d", engine_size*10+num_accidents*100);
	endfunction
endclass
