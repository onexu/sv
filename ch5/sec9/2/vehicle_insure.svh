interface class vehicle_insure;
	pure virtual function int unsigned get_engine_size(); // 获取排量
	pure virtual function int unsigned get_num_accidents(); // 获取事故数
endclass
