class car implements vehicle_drive, vehicle_insure;
	int unsigned engine_size; // 排量
	int unsigned num_accidents; // 事故数

	constraint no_accidents_cons {num_accidents < 256;}
	
	function new(input int unsigned engine_size, input int unsigned num_accidents);
		this.engine_size = engine_size; // 初始化排量
		this.num_accidents = num_accidents; // 初始化事故数
	endfunction

	// 重定义接口类insureable_ifc的所有纯虚方法
	virtual function int unsigned get_engine_size();
		return engine_size;
	endfunction
 
	virtual function int unsigned get_num_accidents();
		return num_accidents;
	endfunction
 
	// 重定义接口类vehicle_drive的所有纯虚方法
	virtual function void accelerate();
		$display("I'm accelerating");
	endfunction
 
	virtual function void turn_left();
		$display("I'm turning left");
	endfunction
 
	virtual function void turn_right();
		$display("I'm turning right");
	endfunction
 
	virtual function void brake();
		$display("I'm braking");
	endfunction
endclass
