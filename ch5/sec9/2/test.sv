`include "vehicle_drive.svh"
`include "vehicle_insure.svh"
`include "car.svh"
`include "driver.svh"
`include "insurer.svh"

module automatic test;
	initial begin
		car audi = new(3, 5); // 排量为3，事故数为5
		driver peter = new(audi);
		insurer andy = new(audi);
		peter.drive();
		andy.insure();
	end
endmodule
