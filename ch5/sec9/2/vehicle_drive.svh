interface class vehicle_drive;
	pure virtual function void accelerate(); // 加速
	pure virtual function void turn_left(); // 左转
	pure virtual function void turn_right(); // 右转
	pure virtual function void brake(); // 刹车
endclass
