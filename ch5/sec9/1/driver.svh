class driver;
	vehicle_drive vd;
	
	function new(vehicle_drive vd);
		this.vd = vd;
	endfunction

	virtual function void drive();
		vd.accelerate();
		vd.turn_left();
		vd.turn_right();
		vd.brake();
	endfunction
endclass
