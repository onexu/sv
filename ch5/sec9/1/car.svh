class car implements vehicle_drive;
	virtual function void accelerate();
		$display("I'm accelerating");
	endfunction
 
	virtual function void turn_left();
		$display("I'm turning left");
	endfunction
 
	virtual function void turn_right();
		$display("I'm turning right");
	endfunction
 
	virtual function void brake();
		$display("I'm braking");
	endfunction
endclass
