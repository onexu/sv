`include "vehicle_drive.svh"
`include "car.svh"
`include "driver.svh"

module automatic test;
	initial begin
		car audi = new();
		driver peter = new(audi);
		peter.drive();
	end
endmodule
