import uvm_pkg::*;
`include "uvm_macros.svh"

`include "observer.svh"
`include "subject.svh"
`include "scoreboard.svh"
`include "monitor.svh"

module test();
	initial begin
		monitor mon = new("mon");
		scoreboard scb0 = new("scb0");
		scoreboard scb1 = new("scb1");

		mon.register_observer(scb0);
		mon.register_observer(scb1);

		`uvm_info("test", "setting monitor state to 10", UVM_LOW)
		mon.set_state(10);

		mon.remove_observer(scb1);
		`uvm_info("test", "setting monitor state to 20", UVM_LOW)
		mon.set_state(20);
	end
endmodule
