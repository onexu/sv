class monitor extends uvm_monitor implements subject;
	observer observers[$];
	int state;

	extern function new(string name = "monitor", uvm_component parent = null);

	virtual function void register_observer(observer o);
		observers.push_back(o);
	endfunction

	virtual function void remove_observer(observer o);
		int idx;
		foreach (observers[idx]) begin
			if (observers[idx] == o) begin
				observers.delete(idx);
				break;
			end
		end
	endfunction

	virtual function void notify_observers();
		foreach (observers[idx]) begin
			observers[idx].update(state);
		end
	endfunction

	virtual function void set_state(int state);
		this.state = state;
		notify_observers();
	endfunction

	virtual function int get_state();
		return state;
	endfunction
endclass

function monitor::new(string name, uvm_component parent);
	super.new(name, parent);
endfunction
