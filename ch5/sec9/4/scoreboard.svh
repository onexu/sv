class scoreboard extends uvm_component implements observer;
	int state;

	extern function new(string name = "scoreboard", uvm_component parent = null);

	virtual function void update(int state);
		this.state = state;
		`uvm_info(get_type_name(), $sformatf("scoreboard state updated to %0d", state), UVM_LOW)
	endfunction
endclass

function scoreboard::new(string name, uvm_component parent);
	super.new(name, parent);
endfunction
