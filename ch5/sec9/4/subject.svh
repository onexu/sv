interface class subject;
	pure virtual function void register_observer(observer o);
	pure virtual function void remove_observer(observer o);
	pure virtual function void notify_observers();
endclass
