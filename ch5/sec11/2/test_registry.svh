class test_registry;
	static vmm_test registry[string]; // 测试登记表保存测试对象句柄

	static function void register(string name, vmm_test t);
		$display("%s(%0d) register class %s", `__FILE__, `__LINE__, name);
		registry[name] = t; // 将测试对象句柄保存到关联数组，索引名为测试类名
	endfunction

	static function vmm_test get_test();
		string name;
		if(!$value$plusargs("VMM_TESTNAME=%s", name)) // 从命令行参数获取测试类名
			$fatal("%s(%0d) +VMM_TESTNAME not found.", `__FILE__, `__LINE__);
		$display("%s(%0d) running test %s...", `__FILE__, `__LINE__, name);
		return registry[name]; // 访问关联数组，返回对应的测试对象句柄
	endfunction
endclass
