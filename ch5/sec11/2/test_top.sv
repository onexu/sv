`include "vmm_test.svh"
`include "test_registry.svh"
`include "test0.svh"
`include "test1.svh"

module test_top;
	vmm_test t;
	initial begin
		t = test_registry::get_test(); // 根据命令行参数从测试登记表获取测试对象句柄
		t.run();
	end
endmodule
