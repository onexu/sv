class test0 extends vmm_test;
	function new();
		$display("invoking %m");
		test_registry::register("test0", this); // 将测试对象句柄注册到测试登记表中
	endfunction

	virtual task run();
		$display("invoking %m");
	endtask
endclass

test0 t0 = new(); // 在编译阶段创建全局测试对象
