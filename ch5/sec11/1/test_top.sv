`include "vmm_test.svh"
`include "test0.svh"
`include "test1.svh"

module test_top;
	vmm_test t;
	initial begin
		t = t0; // t = t1; 手动指定一个测试对象
		t.run();
	end
endmodule
