`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver extends component_base;
	virtual input_intf vif;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void driver::build();
endfunction

function void driver::connect();
endfunction

task driver::main();
	// 输出初始值
	vif.drv_cb.data <= 8'b0;
	vif.drv_cb.valid <= 1'b0;
	// 等待复位结束
	@(posedge vif.rst_n);
	// 发送激励
	repeat(8) begin
		@vif.drv_cb;
		vif.drv_cb.data <= $urandom_range(0, 255);
		vif.drv_cb.valid <= 1'b1;
		$display("driver: data is drived");
	end
	@vif.drv_cb;
	vif.drv_cb.valid <= 1'b0;
endtask

`endif
