`include "dut.sv"
`include "env_pkg.sv"
`include "test.sv"

module top_tb;
	bit clk;
	bit rst_n;
	input_intf input_if(clk, rst_n);
	output_intf output_if(clk, rst_n);

	initial begin // 产生时钟信号
		clk = 1'b0;
		forever #50 clk = ~clk;
	end
	
	initial begin // 产生复位信号
		rst_n = 1'b0;
		#30 rst_n = 1'b1;
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	test i_test(input_if);
	
	dut my_dut(
		.clk(clk),
		.rst_n(rst_n),
		.rxd(input_if.data),
		.rx_dv(input_if.valid),
		.txd(output_if.data),
		.tx_en(output_if.valid));
endmodule
