`ifndef TEST_SV
`define TEST_SV

module automatic test (input_intf input_if);
	import env_pkg::*;
	driver drv;
	initial begin
		drv = new();
		drv.vif = input_if;
		drv.build();
		drv.connect();
		drv.main();
		$finish();
	end
endmodule

`endif
