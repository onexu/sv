`ifndef ENVIRONMENT_SVH
`define ENVIRONMENT_SVH

class environment #(type T = trans_base) extends component_base;
	driver #(T) drv;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void environment::build();
	drv = new();
	drv.build();
endfunction

function void environment::connect();
	drv.connect();
endfunction

task environment::main();
	drv.main();
endtask

`endif
