`ifndef INTF_SVH
`define INTF_SVH

interface input_intf(input clk, input rst_n);
	logic [7:0] data;
	logic valid;

	clocking drv_cb @(posedge clk);
		output data, valid;
	endclocking
endinterface

interface output_intf(input clk, input rst_n);
	logic [7:0] data;
	logic valid;

	clocking mon_cb @(posedge clk);
		input data, valid;
	endclocking
endinterface

`endif
