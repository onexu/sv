`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver #(type T = trans_base) extends component_base;
	virtual input_intf vif;
	mailbox #(T) drv2scb; // 定义信箱句柄
	mailbox #(T) sqr2drv;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
	extern virtual task drive_one_pkt(T tr);
endclass

function void driver::build();
	drv2scb = new(); // 创建信箱实例
endfunction

function void driver::connect();
	svm_config_db#(virtual input_intf)::get("input_vif", vif);
	svm_config_db#(mailbox #(T))::set("drv2scb", drv2scb); // 注册信箱句柄
	svm_config_db#(mailbox #(T))::get("sqr2drv", sqr2drv);
endfunction

task driver::main(); // 向接口发送事务对象
	T tr;
	// 输出初始值
	vif.drv_cb.data <= 8'b0;
	vif.drv_cb.valid <= 1'b0;
	// 等待复位结束
	@(posedge vif.rst_n);
	// 获取激励并发送
	forever begin 
		sqr2drv.get(tr);
		drive_one_pkt(tr);
		drv2scb.put(tr); // 转发transaction对象
	end
endtask

task driver::drive_one_pkt(T tr);
	bit [7:0] data[], t[];

	t = {<<bit[7:0]{tr.dmac}}; // 将dmac保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.smac}}; // 将smac保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.ether_type}}; // 将ether_type保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.pload}}; // 将payload保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.crc}}; // 将crc保存到data
	data = {data, t};

	// 发送激励
	foreach(data[i]) begin
		@vif.drv_cb;
		vif.drv_cb.valid <= 1'b1;
		vif.drv_cb.data <= data[i];
	end
	@vif.drv_cb;
	vif.drv_cb.valid <= 1'b0;
	$display("driver sends one package");
	tr.print();
endtask

`endif
