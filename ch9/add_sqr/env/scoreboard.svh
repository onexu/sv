`ifndef SCOREBOARD_SVH
`define SCOREBOARD_SVH

class scoreboard #(type T = trans_base) extends component_base;
	mailbox #(T) drv2scb, mon2scb; // 定义信箱句柄

	int unsigned pkt_amount;
	T expect_queue[$];

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void scoreboard::build();
endfunction

function void scoreboard::connect();
	svm_config_db#(mailbox #(T))::get("drv2scb", drv2scb);
	svm_config_db#(mailbox #(T))::get("mon2scb", mon2scb);
endfunction

// 比较实际结果和期望结果
task scoreboard::main();
	T get_expect, get_actual;
	bit result;
	fork 
		forever begin
			drv2scb.get(get_expect);
			mon2scb.get(get_actual);
			result = get_actual.compare(get_expect);
			if(result == 1) begin 
				$display("Compare successfully");
			end
			else begin
				$display("Compare failed");
				$display("the expect pkt is");
				get_expect.print();
				$display("the actual pkt is");
				get_actual.print();
			end
			pkt_amount++;
		end
	join_none
endtask

`endif
