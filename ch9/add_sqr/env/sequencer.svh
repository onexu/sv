`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = trans_base) extends component_base;
	mailbox #(T) sqr2drv; // 定义信箱
	T blueprint;
	rand int unsigned pkt_amount;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void sequencer::build();
	sqr2drv = new();
	pkt_amount = $urandom_range(4, 8);
endfunction

function void sequencer::connect();
	svm_config_db#(T)::get("blueprint", blueprint);
	svm_config_db#(mailbox #(T))::set("sqr2drv", sqr2drv);
endfunction

task sequencer::main();
	T tr;
	repeat(pkt_amount) begin
		assert(blueprint.randomize());
		tr = new();
		tr.copy(blueprint);
		sqr2drv.put(tr);
	end
endtask

`endif
