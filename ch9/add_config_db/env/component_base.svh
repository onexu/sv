`ifndef COMPONENT_BASE_SVH
`define COMPONENT_BASE_SVH

virtual class component_base;
	pure virtual function void build();
	pure virtual function void connect();
	pure virtual task main();
endclass
	
`endif
