`ifndef TEST_SV
`define TEST_SV

module automatic test();
	import env_pkg::*;
	driver drv;
	initial begin
		drv = new();
		drv.build();
		drv.connect();
		drv.main();
		$finish();
	end
endmodule

`endif
