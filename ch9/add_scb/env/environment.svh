`ifndef ENVIRONMENT_SVH
`define ENVIRONMENT_SVH

class environment #(type T = trans_base) extends component_base;
	agent #(T) iagt;
	agent #(T) oagt;
	scoreboard #(T) scb;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void environment::build();
	iagt = new();
	oagt = new();
	scb = new();

	iagt.is_active = 1;
	oagt.is_active = 0;
	iagt.build();
	oagt.build();
	scb.build();
endfunction

function void environment::connect();
	iagt.connect();
	oagt.connect();
	scb.connect();
endfunction

task environment::main();
	fork 
		iagt.main();
		oagt.main();
		scb.main();
	join_none
	wait(iagt.drv.pkt_amount == scb.pkt_amount);
endtask

`endif
