`ifndef DRIVER_SVH
`define DRIVER_SVH

class driver #(type T = trans_base) extends component_base;
	virtual input_intf vif;
	mailbox #(T) drv2scb; // 定义信箱句柄
	T blueprint;
	rand int unsigned pkt_amount;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
	extern virtual task drive_one_pkt(T tr);
endclass

function void driver::build();
	drv2scb = new(); // 创建信箱实例
	pkt_amount = $urandom_range(1, 8);
endfunction

function void driver::connect();
	svm_config_db#(virtual input_intf)::get("input_vif", vif);
	svm_config_db#(mailbox #(T))::set("drv2scb", drv2scb); // 注册信箱句柄
	svm_config_db#(T)::get("blueprint", blueprint);
endfunction

task driver::main();
	T tr;
	// 输出初始值
	vif.drv_cb.data <= 8'b0;
	vif.drv_cb.valid <= 1'b0;
	// 等待复位结束
	@(posedge vif.rst_n);
	// 产生激励并发送
	repeat(pkt_amount) begin 
		assert(blueprint.randomize());
		tr = new();
		tr.copy(blueprint);
		drive_one_pkt(tr);
		drv2scb.put(tr); // 转发transaction对象
	end
endtask

task driver::drive_one_pkt(T tr);
	bit [7:0] data[], t[];

	t = {<<bit[7:0]{tr.dmac}}; // 将dmac保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.smac}}; // 将smac保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.ether_type}}; // 将ether_type保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.pload}}; // 将payload保存到data
	data = {data, t};
	t = {<<bit[7:0]{tr.crc}}; // 将crc保存到data
	data = {data, t};

	// 发送激励
	foreach(data[i]) begin
		@vif.drv_cb;
		vif.drv_cb.valid <= 1'b1;
		vif.drv_cb.data <= data[i];
	end
	@vif.drv_cb;
	vif.drv_cb.valid <= 1'b0;
	$display("driver sends one package");
	tr.print();
endtask

`endif
