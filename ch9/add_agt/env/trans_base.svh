`ifndef TRANS_BASE_SVH
`define TRANS_BASE_SVH

virtual class trans_base;
	pure virtual function void copy(input trans_base rhs = null);
	pure virtual function void print();
endclass

`endif
