`ifndef AGENT_SVH
`define AGENT_SVH

class agent #(type T = trans_base) extends component_base;
	bit is_active; // iagent或oagent
	driver #(T) drv;
	monitor #(T) mon;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass
	
function void agent::build();
	if(is_active == 1) begin
		drv = new();
		drv.build();
	end
	else begin
		mon = new();
		mon.build();
	end
endfunction

function void agent::connect();
	if(is_active == 1)
		drv.connect();
	else
		mon.connect();
endfunction

task agent::main();
	if(is_active == 1'b1)
		drv.main();
	else
		mon.main();
endtask

`endif
