`ifndef ENV_PKG_SV
`define ENV_PKG_SV

`include "intf.svh"

package env_pkg;
	import svm_pkg::*;
	`include "trans_base.svh"
	`include "transaction.svh"
	`include "component_base.svh"
	`include "driver.svh"
	`include "monitor.svh"
	`include "environment.svh"
endpackage

`endif
