`ifndef ENVIRONMENT_SVH
`define ENVIRONMENT_SVH

class environment #(type T = trans_base) extends component_base;
	driver #(T) drv;
	monitor #(T) mon;

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
endclass

function void environment::build();
	drv = new();
	mon = new();
	drv.build();
	mon.build();
endfunction

function void environment::connect();
	drv.connect();
	mon.connect();
endfunction

task environment::main();
	fork
		drv.main();
		mon.main();
	join_none
	wait(drv.pkt_amount == mon.pkt_amount);
endtask

`endif
