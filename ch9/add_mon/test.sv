`ifndef TEST_SV
`define TEST_SV

module automatic test();
	import svm_pkg::*;
	import env_pkg::*;
	transaction blueprint;
	environment #(transaction) env;
	initial begin
		blueprint = new();
		env = new();
		blueprint = new();
		svm_config_db#(transaction)::set("blueprint", blueprint);
		env.build();
		env.connect();
		env.main();
		$finish();
	end
endmodule

`endif
