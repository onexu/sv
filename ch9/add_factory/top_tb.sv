`include "dut.sv"
`include "svm_pkg.sv"
`include "env_pkg.sv"
`include "test_pkg.sv"

module top_tb;
	import svm_pkg::*;
	import env_pkg::*;
	bit clk;
	bit rst_n;
	input_intf input_if(clk, rst_n);
	output_intf output_if(clk, rst_n);
	
	initial begin
		clk = 1'b0;
		forever #50 clk = ~clk;
	end
	
	initial begin
		rst_n = 1'b0;
		#30 rst_n = 1'b1;
	end
	
	initial begin
		svm_config_db #(virtual input_intf)::set("input_vif", input_if);
		svm_config_db #(virtual output_intf)::set("output_vif", output_if);
	end

	initial begin
		svm_component test_obj;
		test_obj = svm_factory::get_test();
		test_obj.run_test();
		$finish();
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	dut my_dut(
		.clk(clk),
		.rst_n(rst_n),
		.rxd(input_if.data),
		.rx_dv(input_if.valid),
		.txd(output_if.data),
		.tx_en(output_if.valid));
endmodule
