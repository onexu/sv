`ifndef MONITOR_SVH
`define MONITOR_SVH

class monitor #(type T = trans_base) extends component_base;
	virtual output_intf vif; // 定义虚接口	
	mailbox #(T) mon2scb; // 定义信箱句柄

	extern virtual function void build();
	extern virtual function void connect();
	extern virtual task main();
	extern virtual task collect_one_pkt(T tr);
endclass

function void monitor::build();
	mon2scb = new(); // 创建信箱实例
endfunction

function void monitor::connect();
	svm_config_db#(virtual output_intf)::get("output_vif", vif); // 注册信箱句柄
	svm_config_db#(mailbox #(T))::set("mon2scb", mon2scb);
endfunction

task monitor::main();
	forever begin
		T tr;
		tr = new();
		collect_one_pkt(tr);
		mon2scb.put(tr); // 发送transaction对象
	end
endtask

task monitor::collect_one_pkt(T tr);
	bit[7:0] data_q[$];
	int psize;

	do
		@vif.mon_cb;
	while(vif.mon_cb.valid !== 1);

	do
	begin
		data_q.push_back(vif.mon_cb.data);
		@vif.mon_cb;
	end
	while(vif.mon_cb.valid == 1'b1);

	tr.dmac = {<<bit[7:0]{data_q[0:5]}}; // 从队列中取出dmac
	tr.smac = {<<bit[7:0]{data_q[6:11]}}; // 从队列中取出smac
	tr.ether_type = {<<bit[7:0]{data_q[12:13]}}; // 从队列中取出ether_type
	tr.pload  = {<<bit[7:0]{data_q[14:$-4]}}; // 从队列中取出payload
	tr.crc = {<<bit[7:0]{data_q[$-3:$]}}; // 从队列中取出crc

	$display("output monitor collects one pkt");
	tr.print();
endtask

`endif
