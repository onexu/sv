`ifndef CALLBACK_SVH
`define CALLBACK_SVH

virtual class callback #(type T = trans_base); // 回调基类
	virtual function void pre_tx(input T rhs);
	endfunction
	virtual function void post_tx(input T rhs);
	endfunction
endclass

class callback_drv_set_crc #(type ET = trans_base) extends callback;
	virtual function void pre_tx(input T rhs);
		ET rhs_;
		if(rhs == null)
			$display("error: rhs is null");
		assert($cast(rhs_, rhs));
		if (!$urandom_range(0,4)) begin; // 以20%概率设置事务对象中的crc值
			rhs_.crc = 32'hffff;
			$display("crc set");
		end
	endfunction
endclass

class callback_drv_get_cov #(type ET = trans_base) extends callback;
	ET rhs_;

	covergroup cov_ether_type;
		coverpoint rhs_.ether_type; // 覆盖点
		option.at_least = 1;
	endgroup

	function new();
		cov_ether_type = new();
	endfunction

	virtual function void post_tx(input T rhs);
		if(rhs == null)
			$display("error: rhs is null");
		assert($cast(rhs_, rhs));
		cov_ether_type.sample(); // 收集覆盖
	endfunction
endclass

`endif
