`ifndef TEST_BASE_SVH
`define TEST_BASE_SVH

class test_base extends svm_component;
	transaction blueprint;
	environment #(transaction) env;
	`svm_component_utils(test_base);

	function new(string name);
		super.new(name);
		blueprint = new();
		env = new();
	endfunction

	virtual task run_test();
		svm_config_db#(transaction)::set("blueprint", blueprint);
	endtask
endclass

`endif
