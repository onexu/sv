`ifndef TEST_COV_SVH
`define TEST_COV_SVH

class test_cov extends test_base;
	callback_drv_set_crc #(transaction) cb_drv_set_crc;
	callback_drv_get_cov #(transaction) cb_drv_get_cov;
	`svm_component_utils(test_cov);

	function new(string name);
		super.new(name);
		cb_drv_set_crc = new();
		cb_drv_get_cov = new();
	endfunction

	virtual task run_test();
		super.run_test();
		env.build();
		env.iagt.drv.drv_cb.push_back(cb_drv_set_crc); // 将回调方法加入到回调队列中
		env.iagt.drv.drv_cb.push_back(cb_drv_get_cov); // 将回调方法加入到回调队列中
		env.connect();
		env.main();
	endtask
endclass

`endif
