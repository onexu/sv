`ifndef TEST_NORMAL_SV
`define TEST_NORMAL_SV

class test_normal extends test_base;
	`svm_component_utils(test_normal);

	function new(string name);
		super.new(name);
	endfunction

	virtual task run_test();
		super.run_test();
		env.build();
		env.connect();
		env.main();
	endtask
endclass

`endif
