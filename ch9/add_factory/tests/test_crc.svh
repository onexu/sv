`ifndef TEST_CRC_SVH
`define TEST_CRC_SVH

class test_crc extends test_base;
	callback_drv_set_crc #(transaction) cb_drv_set_crc;
	`svm_component_utils(test_crc);

	function new(string name);
		super.new(name);
		cb_drv_set_crc = new();
	endfunction

	virtual task run_test();
		super.run_test();
		env.build();
		env.iagt.drv.drv_cb.push_back(cb_drv_set_crc); // 将回调方法加入到回调队列中
		env.connect();
		env.main();
	endtask
endclass

`endif
