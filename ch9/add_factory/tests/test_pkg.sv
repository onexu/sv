`ifndef TEST_PKG_SV
`define TEST_PKG_SV

package test_pkg;
	import svm_pkg::*;
	import env_pkg::*;
	`include "test_base.svh"
	`include "test_normal.svh"
	`include "test_crc.svh"
	`include "test_cov.svh"
endpackage

`endif
