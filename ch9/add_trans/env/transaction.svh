`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction extends trans_base;
	rand bit[47:0] dmac;
	rand bit[47:0] smac;
	rand bit[15:0] ether_type;
	rand bit[7:0]  pload[];
	rand bit[31:0] crc;

	constraint pload_cons{
		pload.size inside {[1:8]};
	}

	function void post_randomize();
		crc = calc_crc;
	endfunction

	virtual function bit[31:0] calc_crc();
		return 32'h0;
	endfunction

	virtual function void print();
		$display("dmac=%0h", dmac);
		$display("smac=%0h", smac);
		$display("ether_type=%0h", ether_type);
		$display("pload=%0p", pload);
		$display("crc=%0h", crc);
	endfunction

	virtual function void copy(input trans_base rhs = null);
		transaction rhs_;
		if ((rhs == null) || !$cast(rhs_, rhs))
			$display("copy fail");
		dmac = rhs_.dmac;
		smac = rhs_.smac;
		ether_type = rhs_.ether_type;
		pload = new[rhs_.pload.size];
		for(int i = 0; i < pload.size(); i++) begin
			pload[i] = rhs_.pload[i];
		end
		crc = rhs_.crc;
	endfunction
endclass

`endif
