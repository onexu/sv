`ifndef TEST_SV
`define TEST_SV

module automatic test();
	import svm_pkg::*;
	import env_pkg::*;
	transaction blueprint;
	driver #(transaction) drv;
	initial begin
		blueprint = new();
		drv = new();
		svm_config_db#(transaction)::set("blueprint", blueprint);
		drv.build();
		drv.connect();
		drv.main();
		$finish();
	end
endmodule

`endif
