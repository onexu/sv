`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit a;
	rand bit [1:0] b;

	constraint cons { (a == 0) -> (b == 0); }
	constraint cons_order { solve a before b; }

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
