`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		int cnt[5];
		tr = new();
		repeat (10000) begin
			assert(tr.randomize());
			case({tr.a, tr.b})
				3'b000: cnt[0]++;
				3'b100: cnt[1]++;
				3'b101: cnt[2]++;
				3'b110: cnt[3]++;
				3'b111: cnt[4]++;
			endcase
		end
		$display("%0p", cnt);
	end
endmodule

`endif
