`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();

		tr.a_cons.constraint_mode(0); // 关闭约束块a_cons
		repeat (16) begin
			assert(tr.randomize());
			tr.print();
		end

		tr.constraint_mode(0); // 关闭所有约束块
		repeat (16) begin
			assert(tr.randomize());
			tr.print();
		end

		$display("tr.constraint_mode=%0h", tr.a_cons.constraint_mode());
	end
endmodule

`endif
