`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();
		repeat (4) begin
			assert(tr.randomize());
			tr.print();
		end
	end
endmodule

`endif
