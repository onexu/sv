`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class config_info;
	rand bit [3:0] b; // 随机属性
endclass

class transaction;
	rand bit [3:0] a; // 随机属性
	rand config_info cfg_info; // 随机句柄不可定义成randc类型

	function new(input bit [3:0] a = 0);
		this.a = a;
		cfg_info = new();
	endfunction

	function void print(string name = "");
		$display("%s: a=%0h, %0p", name, a, cfg_info);
	endfunction
endclass

`endif
