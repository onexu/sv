`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();
		assert(tr.randomize());
		tr.print();
		assert(tr.randomize(a)); // 只随机化属性a
		tr.print();
		tr.b = 0; // 让随机属性b的取值不满足约束条件
		assert(tr.randomize(null)); // 检查随机属性是否满足约束
	end
endmodule

`endif
