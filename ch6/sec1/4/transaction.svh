`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a;
	randc bit [3:0] b;
	
	constraint a_cons { a < b;}
	constraint b_cons { b > 7;}

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
