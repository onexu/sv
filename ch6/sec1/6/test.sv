`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();

		tr.rand_mode(0); // 导致随机化失败
		repeat (16) begin
			assert(tr.randomize());
			tr.print();
		end

		tr.rand_mode(1);
		tr.a.rand_mode(0);
		repeat (16) begin
			assert(tr.randomize());
			tr.print();
		end

		$display("a.rand_mode=%0h", tr.a.rand_mode());
	end
endmodule

`endif
