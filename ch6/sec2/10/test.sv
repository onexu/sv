`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	bit [3:0] a,b;
	initial begin
		transaction tr;
		tr = new();	
		tr.constraint_mode(0);
		a = $urandom_range(0, 15);
		b = $urandom_range(0, 15);
		$display("local var: a=%0d, b=%0d", a, b);
		repeat (16) begin
			assert(tr.randomize() with { a < local::a; b < local::b; });
			tr.print();
		end
	end
endmodule

`endif
