`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();
		repeat (16) begin
			tr.mode = $urandom_range(0, 3);
			assert(tr.randomize());
			tr.print();
		end
	end
endmodule

`endif
