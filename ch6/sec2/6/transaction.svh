`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a, b;
	bit[1:0] mode;

	constraint cons { mode -> a == 0; }

	virtual function void print(string name = "");
		$display("%s: mode=%0d, a=%0d, b=%0d", name, mode, a, b);
	endfunction
endclass

`endif
