`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new('{0,1,2,3});
		repeat(10) begin
			assert(tr.randomize());
			tr.print();
		end
	end
endmodule

`endif
