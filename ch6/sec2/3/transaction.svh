`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a, b;
	bit [3:0] array[];
	
	function new(input bit [3:0] array[]);
		this.array = array;
	endfunction

	constraint a_cons { a inside {array}; } // 数组作为inside的集合成员

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
