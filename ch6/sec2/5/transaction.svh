`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a, b, excluded;

	constraint uni { unique {a, b, excluded}; }
	constraint exclusion { excluded == 5; }

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
