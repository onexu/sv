`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [1:0] a, b; // 随机属性

	int d1 = 40, d2 = 60; // 权重属性

	constraint cons
	{
		a dist {0:=40, [1:3]:=60};
		b dist {0:/d1, [1:3]:/d2};
	}
	
	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
