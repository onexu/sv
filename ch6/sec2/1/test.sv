`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		int cnt[2][4];
		tr = new();
		repeat (22000) begin
			assert(tr.randomize());
			case(tr.a)
				2'h0: cnt[0][0]++;
				2'h1: cnt[0][1]++;
				2'h2: cnt[0][2]++;
				2'h3: cnt[0][3]++;
			endcase
			case(tr.b)
				2'h0: cnt[1][0]++;
				2'h1: cnt[1][1]++;
				2'h2: cnt[1][2]++;
				2'h3: cnt[1][3]++;
			endcase
		end
		$display("%0p", cnt[0]);
		$display("%0p", cnt[1]);
	end
endmodule

`endif
