`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new('{1,3,5,7,9,11,13,15});
		repeat (tr.array.size()) begin
			assert(tr.randomize());
			tr.print();
		end
	end
endmodule

`endif
