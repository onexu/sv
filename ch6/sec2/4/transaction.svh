`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	bit [3:0] array[];
	randc bit [3:0] index; // 数组索引

	function new(input bit [3:0] array[]);
		this.array = array;
	endfunction

	constraint cons_size { index < array.size(); }

	virtual function void print(string name = "");
		$display("%s: array[%0d]=%0d", name, index, array[index]);
	endfunction
endclass

`endif
