`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a,b;
	bit[1:0] mode;

	constraint cons
	{
		if(mode == 1) a == 0;
		else if(mode == 0) a == 4'hf;
	}

	virtual function void print(string name = "");
		$display("%s: mode=%0d, a=%0d, b=%0d", name, mode, a, b);
	endfunction
endclass

`endif
