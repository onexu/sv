`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a, b;
	bit [3:0] lo, hi;

	constraint cons_range
	{
		a inside {0, 2, 4, 6, 8, 10, 12, 14};
		!(b inside {[lo:hi]});
	}

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif