`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();
		repeat(16) begin
			tr.lo = $urandom_range(0, 5);
			tr.hi = $urandom_range(10, 15);
			assert(tr.randomize());
			tr.print();
		end
	end
endmodule

`endif
