`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();

		repeat (16) begin
			assert(tr.randomize() with { a == b; });
			tr.print();
		end
	end
endmodule

`endif
