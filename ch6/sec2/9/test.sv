`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	initial begin
		transaction tr;
		tr = new();	
		tr.constraint_mode(0);
		repeat (16) begin
			assert(tr.randomize() with { a < 8; b < 10; });
			tr.print();
		end
	end
endmodule

`endif