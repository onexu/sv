`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	static int unsigned count;
	rand bit [3:0] a, b;
	bit [3:0] lo, hi;

	constraint cons_range {	a inside {[lo:hi]}; }

	function void pre_randomize();
		lo = $urandom_range(0, 5);
		hi = $urandom_range(10, 15);
	endfunction

	function void post_randomize();
		$display ("The %0dth randomization", count++);
	endfunction

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
