`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a[];

	constraint size_cons { a.size() inside {[1:10]}; }
	constraint sum_cons { a.sum() with (int'(item)) == 35; }

	virtual function void print(string name = "");
		$display("%s: a=%0p", name, a);
	endfunction
endclass

`endif
