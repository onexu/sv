`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a[];

	constraint size_cons { a.size() inside {[1:10]}; } // 约束数组长度
	constraint data_cons // 约束数组元素取值范围
	{
		foreach (a[i])
			a[i] inside {[0:12]};
	}
	constraint ascend_cons // 约束数组元素值递增
	{
		foreach (a[i])
			if (i > 0)
				a[i] > a[i-1];
	}
	virtual function void print(string name = "");
		$display("%s: a=%0p", name, a);
	endfunction
endclass

`endif
