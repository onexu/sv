`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a[4];

	constraint size_cons { a.size() inside {[1:10]}; } // 约束数组长度
	constraint data_cons // 约束数组元素取值范围
	{
		foreach (a[i])
			a[i] inside {[0:12]};
	}
	constraint unique_cons { unique {a}; } // 约束数组元素不重复取值

	virtual function void print(string name = "");
		$display("%s: a=%0p", name, a);
	endfunction
endclass

`endif
