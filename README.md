### SystemVerilog数字集成电路功能验证

#### 介绍
本书源代码、课件、开发环境等资源仅限于学习交流。源代码和书中内容会保持更新，书中存在的不足和纰漏见勘误及更新。

#### 开发环境
关注微信公众号[学验证]获取下载地址，虚拟机用户eda无密码，用户root密码：1111。

#### 启动授权
使用用户eda登录，打开terminal。
```bash
cd ~/flexlm
lmgrd -c synopsys.dat -l log.txt
```

#### 运行例子
1. 进入sv目录，子目录svm-1.1和svm-1.2包含了配置数据库和工厂机制的包。
2. 第5章和第9章的部分例子需要导入目录svm1.1或svm1.2中的包，Bash Shell下执行``source setting.sh``，TC Shell执行``source setting.csh``，设置的环境变量``$SRC_HOME``和``$SVM_HOME``将在例子中的filelist.f中使用。
3. 将目录sv下的Makefile复制到某个例子所在目录，然后输入make执行仿真。
4. Makefile的详细使用说明见教材附录A。

假定sv目录位于~下。

例1：仿真sv/ch2/sec1/1下的源文件。
```bash
cd ~/sv/ch2/sec1/1
cp ~/sv/Makefile .
make
```

例2：仿真sv/ch5/sec11/2下的源文件。
```bash
cd ~/sv
source setting.sh
cd ch5/sec11/2
cp ~/sv/Makefile .
make
```