`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input logic clk,
	input logic rst_n);

	logic a, b, c;

	initial begin
		a <= 0;
		@(posedge clk) a <= 1;
		repeat(6) @(posedge clk) a <= 0;
		$finish();
	end

	initial begin
		b <= 0;
		repeat($urandom_range(1,4)) @(posedge clk) b <= 0;
		@(posedge clk) b <= 1;
		@(posedge clk) b <= 0;
	end

	initial begin
		c <= 0;
		repeat(3) @(posedge clk) c <= 0;
		@(posedge clk) c <= 1;
		@(posedge clk) c <= 0;
	end

	sequence seq0;
		a ##[1:4] b; // 延迟1~4个时钟周期
	endsequence
	
	sequence seq1;
		a ##3 c; // 延迟3个时钟周期
	endsequence
	
	property prop0;
		@(posedge clk)
		disable iff (!rst_n)
		seq0 and seq1;
	endproperty
	
	property prop1;
		@(posedge clk)
		disable iff (!rst_n)
		seq0 intersect seq1;
	endproperty
	
	property prop2;
		@(posedge clk)
		disable iff (!rst_n)
		seq0 or seq1;
	endproperty
	
	assert0: assert property(prop0)
		$display("@%0t assert0 passed.", $time);

	assert1: assert property(prop1)
		$display("@%0t assert1 passed.", $time);

	assert2: assert property(prop2)
		$display("@%0t assert2 passed.", $time);
endmodule

`endif
