`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input  logic clk,
	input  logic rst_n,
	input  logic [3:0] grant,
	output logic [3:0] req);

	initial begin // 激励
		req <= 0;
		@(posedge clk) req <= 4'b0001;
		@(posedge clk) req <= 4'b0011;
		@(posedge clk) req <= 4'b0000;
		repeat(3) @(posedge clk) req <= 4'b0;
		$finish;
	end

	sequence req_gnt_seq; // 序列层
		req[0] ##1 grant[0]; // 序列由2个布尔表达式组成
	endsequence
	
	property req_gnt_prop; // 属性层
		@(posedge clk)
		disable iff (!rst_n) // 复位期间不执行
		!grant |=> req_gnt_seq; // 序列由1个布尔表达式和1个子序列组成
	endproperty
	
	req_gnt_assert: assert property(req_gnt_prop) // 断言语句层
		$display("@%0t assertion passed.", $time);
	else
		$error("@%0t assertion failed.", $time);
endmodule

`endif
