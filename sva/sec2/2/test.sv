`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input  logic clk,
	input  logic rst_n,
	input  logic [3:0] grant,
	output logic [3:0] req);

	initial begin // 激励
		req <= 0;
		@(posedge clk) req <= 4'b0001;
		@(posedge clk) req <= 4'b0011;
		@(posedge clk) req <= 4'b0000;
		repeat(3) @(posedge clk) req <= 4'b0;
		$finish;
	end

	property req_prop; // 属性层
		@(posedge clk)
		disable iff (!rst_n) // 复位期间不执行
		req[0] ##1 req[0]; // req连续2个时钟周期有效
	endproperty
	
	req_assert: assume property(req_prop) // 断言语句层
		$display("@%0t assertion passed.", $time);
	else
		$error("@%0t assertion failed.", $time);
endmodule

`endif
