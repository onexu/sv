`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input  logic clk,
	input  logic rst_n,
	input  logic [3:0] grant,
	output logic [3:0] req);

	initial begin // 激励
		req <= 0;
		repeat(2) @(posedge clk) req <= 4'b0001;
		@(posedge clk) req <= 4'b0000;
		$finish;
	end

	always_comb begin
		check_grant0: assert final (!(grant[0] && !req[0])) // 立即断言语句
			$display ("@%0t, assert passed.", $time); // 断言通过执行这里，可以省略
		else // else语句可以省略
			$error("@%0t, grant without request for agent 0.", $time); // 断言失败执行这里
	end
endmodule

`endif
