`include "round_robin_arbiter.sv"
`include "test.sv"

module top_tb();
    bit clk;
    bit rst_n;
    logic [3:0] req;
    logic [3:0] grant;

    initial begin
    	clk = 1'b0;
    	forever #50 clk = ~clk;
    end

    initial begin
    	rst_n = 1'b0;
    	#30 rst_n = 1'b1;
    end

    initial begin
    	$fsdbDumpfile("./wave.fsdb");
    	$fsdbDumpvars("+all");
    	$fsdbDumpMDA(0, top_tb);
    	$fsdbDumpSVA();
    end

    round_robin_arbiter u_round_robin_arbiter(clk, rst_n, req, grant);
    test u_test(clk, rst_n, grant, req);
endmodule