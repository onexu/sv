`include "test.sv"

module top_tb();

bit clk;
bit rst_n;

initial begin
	clk = 0;
	forever #50 clk = ~clk;
end

initial begin
	rst_n = 0;
	#30 rst_n = 1;
end

initial begin
	$fsdbDumpfile("./wave.fsdb");
	$fsdbDumpvars("+all");
	$fsdbDumpMDA(0, top_tb);
	$fsdbDumpSVA();
end

test u_test(clk, rst_n);

endmodule