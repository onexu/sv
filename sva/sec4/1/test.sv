`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input logic clk,
	input logic rst_n);

	logic [3:0] req, grant;

	initial begin
		// 输出初始值
		req <= 'h0;
		grant <= 'h0;
		// 等待复位结束
		@(posedge rst_n);
		// 发送激励并采集响应
		@(posedge clk);
		req <= 'h1;
		repeat(1000) @(posedge clk);
		req <= 'h0;
		grant <= 'h1;
		@(posedge clk);
		grant <= 'h0;
		@(posedge clk);
		$finish();
	end

	bad_assert: assert property ( // 效率低
		@(posedge clk)
		disable iff (!rst_n)
		(req[0] -> grant[0])); // 产生1000个子进程
	
	good_assert: assert property ( // 效率高
		@(posedge clk)
		disable iff (!rst_n)
		($rose(req[0]) -> grant[0])); // 产生1个子进程
endmodule

`endif
