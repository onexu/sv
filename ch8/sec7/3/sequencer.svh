`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // Coverage group
		track_sum: coverpoint (tr.sum)
		{
			bins valid[] = {[0:30]};
		}
	endgroup

	function new();
		cg = new(); // Instantiate coverage group
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // Gather coverage
		end
	endtask
endclass

`endif
