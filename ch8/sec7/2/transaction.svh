`ifndef TRANSACTION_SVH
`define TRANSACTION_SVH

class transaction;
	rand bit [3:0] a, b;
	rand bit [4:0] sum;

	constraint sum_cons
	{
		sum == a + b;
		solve sum before a, b;
	}

	virtual function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
