`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"

module automatic test;
	transaction tr;

	covergroup cg; // 定义覆盖组
		track_a: coverpoint tr.a; // 定义覆盖点
		option.at_least = 1; // 设置覆盖选项
	endgroup

	initial begin
		cg cgi; // 定义覆盖组句柄
		cgi = new(); // 例化覆盖组
		repeat (32) begin
			tr = new();
			assert(tr.randomize());
			tr.print();
			cgi.sample(); // 收集覆盖
		end
	end
endmodule

`endif
