`ifndef TRANSACTION_SV
`define TRANSACTION_SV

class transaction;
	rand bit [3:0] a, b;
	
	function void print(string name = "");
		$display("%s: a=%0d, b=%0d", name, a, b);
	endfunction
endclass

`endif
