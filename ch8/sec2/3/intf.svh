`ifndef INTF_SVH
`define INTF_SVH

interface intf #(parameter WIDTH = 4) (input clk, input rst_n);
	logic [WIDTH-1:0] a;
	logic [WIDTH-1:0] b;
	logic [WIDTH:0] sum;

	clocking cb @(posedge clk); // Clocking block
		output a, b;
		input sum;
	endclocking

	modport TEST(clocking cb, input clk, rst_n);
endinterface

`endif
