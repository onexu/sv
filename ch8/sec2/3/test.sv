`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"
`include "intf.svh"

module automatic test(intf i_intf);
	transaction tr;

	covergroup cg;
		coverpoint tr.a iff (i_intf.rst_n);
	endgroup

	initial begin
		cg cgi;
		cgi = new();
		// 等待复位结束
		@(posedge i_intf.rst_n);
		repeat (32) begin
			cgi.stop(); // 停止覆盖收集
			tr = new();
			assert(tr.randomize());
			tr.print();
			cgi.start(); // 启动覆盖收集
			cgi.sample();
		end
		$finish();
	end
endmodule

`endif
