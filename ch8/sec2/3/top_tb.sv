module top_tb;
	bit clk;
	bit rst_n;
	intf i_intf(clk, rst_n);

	initial begin
        clk = 0;
		forever #50 clk = ~clk;
	end

	initial begin
		rst_n = 1;
		#10 rst_n = 0;
		#20 rst_n = 1;
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	test i_test (i_intf);
endmodule