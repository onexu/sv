`ifndef TEST_SV
`define TEST_SV

`include "transaction.svh"
`include "sequencer.svh"

module automatic test;
	initial begin
		sequencer seqr;
		seqr = new();
		seqr.num_trans = 32;
		seqr.main();
	end
endmodule

`endif
