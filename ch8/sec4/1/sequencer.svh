`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 覆盖组
		track_a: coverpoint tr.a // 覆盖点
		{
			bins zero = {0}; // 为0创建1个仓
			bins lo[3] = {[1:2], 3}; // 为数值1-3创建3个仓
			bins hi[] = {[8:$]}; // 为数值8-15创建8个仓
			bins misc = default; // 创建默认仓
		}
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
