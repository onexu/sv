`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

covergroup cg (ref bit [3:0] a, input int mid);
	option.per_instance = 1;
	coverpoint a
	{
		bins lo = { [0:mid-1] };
		bins hi = { [mid:$] };
	}
endgroup

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;
	cg cga, cgb;

	virtual task main();
		tr = new();
		cga = new(tr.a, 7);
		cgb = new(tr.b, 9);
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cga.sample();
			cgb.sample();
		end
	endtask
endclass

`endif
