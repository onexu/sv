`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 覆盖组
		sum1: coverpoint (tr.a + tr.b + 5'b0) // 覆盖点
		{
			bins all[] = { [0:31] }; // 32个仓
			ignore_bins ignore = {31}; // 忽略仓
		}
		sum2: coverpoint (tr.a + tr.b + 5'b0) // 覆盖点
		{
			option.auto_bin_max = 32; // 32个仓
			illegal_bins ignore = {31}; // 非法仓
		}
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
