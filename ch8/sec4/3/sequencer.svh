`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 覆盖组
		coverpoint tr.a; // 覆盖点
		sum0: coverpoint (tr.a + tr.b + 5'b0); // 32个仓
		sum1: coverpoint (tr.a + tr.b + 5'b0) { bins valid[] = {[0:30]}; } // 31个仓
		sum2: coverpoint (tr.a + tr.b + 5'b0) { bins all[] = {[0:31]} with (item < 31); } // 31个仓
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
