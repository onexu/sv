`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 覆盖组
		track_a: coverpoint tr.a // 覆盖点
		{
			bins to_nozero = (0 => 1), (0 => 2), (0 => 3); // 转换覆盖
			bins to_odd    = (0, 2 => 1, 3); // 转换覆盖
		}
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
