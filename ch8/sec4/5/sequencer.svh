`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 定义覆盖组
		track_a: coverpoint tr.a; // 定义覆盖点
		option.auto_bin_max = 2; // 设置所有覆盖点的最大仓数
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
