`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;
	event done; // 定义事件

	covergroup cg @(done); // 定义覆盖组，将事件加入敏感列表
		track_a: coverpoint tr.a; // 定义覆盖点
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			->>done; // 触发事件
		end
	endtask
endclass

`endif
