`ifndef TEST_SV
`define TEST_SV

interface simple_bus (input bit clk); // 接口
	logic wen;
	logic [3:0] wdata;
endinterface

module automatic test(simple_bus bus);
	covergroup cg; // 定义覆盖组
		coverpoint bus.wdata; // 定义覆盖点
	endgroup

	cg cgi; // 定义覆盖组句柄

	cover property (@(posedge bus.clk) bus.wen == 1) // 断言触发覆盖组
		cgi.sample;

	initial begin
		cgi = new(); // 创建覆盖组实例
		repeat (32) begin
			@(posedge bus.clk);
			bus.wen <= $urandom_range(0, 1);
			bus.wdata <= $urandom_range(0, 15);
			$display("wen=%0d, wdata=%0d", bus.wen, bus.wdata);
		end
		$finish();
	end
endmodule

module top_tb();
	bit clk;

	initial begin
		forever #50 clk = ~clk;
	end	

	simple_bus bus(clk);
	test i_test(bus);
endmodule

`endif
