`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg with function sample(input bit [3:0] a); // 重写sample方法
		track_a: coverpoint a;
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(tr.a); // 收集覆盖
		end
	endtask
endclass

`endif
