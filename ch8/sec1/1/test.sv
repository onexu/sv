`ifndef TEST_SV
`define TEST_SV

module automatic test #(parameter WIDTH = 4) (
	input logic clk,
	input logic rst_n,
	input logic [WIDTH:0] sum,
	output logic [WIDTH-1:0] a,
	output logic [WIDTH-1:0] b);

	initial begin
		// 输出初始值
		a <= 4'h0;
		b <= 4'h0;
		// 等待复位结束
		@(posedge rst_n);
		// 发送激励并采集响应
		repeat(5) begin
			@(posedge clk);
			a <= $urandom_range(0, 4'h7);
			b <= $urandom_range(0, 4'h7);
			$display("@%0t, a=%0d, b=%0d, sum=%0d", $time, a, b, sum);
		end
		$finish();
	end
endmodule

`endif
