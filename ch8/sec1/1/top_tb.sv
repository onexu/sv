`include "dut.sv"
`include "test.sv"

module top_tb;
	bit clk;
	bit rst_n;	
	logic [3:0] a;
	logic [3:0] b;
	logic [4:0] sum;

	initial begin
		clk = 1'b0;
		forever #50 clk = ~clk;
	end

	initial begin
		rst_n = 1'b0;
		#30 rst_n = 1'b1;
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	adder i_adder (
		.clk(clk),
		.rst_n(rst_n),
		.a(a),
		.b(b),
		.sum(sum));

	test i_test (
		.clk(clk),
		.rst_n(rst_n),
		.a(a),
		.b(b),
		.sum(sum));
endmodule