`ifndef DUT_SV
`define DUT_SV

module adder #(parameter WIDTH = 4) (
	input clk,
	input rst_n,
	input [WIDTH-1:0] a,
	input [WIDTH-1:0] b,
	output reg [WIDTH:0] sum);

	always @(posedge clk, negedge rst_n) begin
		sum <= a + b;
	end
endmodule

`endif
