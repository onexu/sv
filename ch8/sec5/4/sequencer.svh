`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg; // 覆盖组
		track_a: coverpoint tr.a // 覆盖点
		{
			bins a0 = {[0:3]};  // 为数值0-3创建1个仓
			bins a1 = {[8:11]}; // 为数值8-11创建1个仓
			bins misc = default; // 默认仓
		}
		track_b: coverpoint tr.b
		{
			bins b0 = {[0:3]};
			bins b1 = {[8:11]};
			bins misc = default;
		}
		cross track_a, track_b
		{
			bins c0 = !binsof(track_a) intersect {[6:$]}; // 注意取反操作符
			bins c1 = binsof(track_a.a0) || binsof(track_b.b1); // 排除<a1,b0>
			bins c2 = binsof(track_a.a0) && binsof(track_b.b1); // 只包含<a0,b1>
		}
	endgroup

	function new();
		cg = new(); // 例化覆盖组
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample(); // 收集覆盖
		end
	endtask
endclass

`endif
