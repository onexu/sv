`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg;
		track_a: coverpoint tr.a;
		track_b: coverpoint tr.b;
		cross track_a, track_b; // 交叉覆盖
	endgroup

	function new();
		cg = new();
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample();
		end
	endtask
endclass

`endif
