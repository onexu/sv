`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg;
		track_a: coverpoint tr.a { option.weight=0; } // 此覆盖点权重为0
		track_b: coverpoint tr.b { option.weight=0; } // 此覆盖点权重为0
		cross_ab: cross track_a, track_b
		{
			bins a0b0 = binsof(track_a) intersect {0} && binsof(track_b) intersect {0};
			bins a1b0 = binsof(track_a) intersect {1} && binsof(track_b) intersect {0};
			bins b1   = binsof(track_b) intersect {1};
		}
	endgroup

	function new();
		cg = new();
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample();
		end
	endtask
endclass

`endif
