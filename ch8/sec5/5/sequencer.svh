`ifndef SEQUENCER_SVH
`define SEQUENCER_SVH

class sequencer #(type T = transaction);
	int unsigned num_trans;
	T tr;

	covergroup cg;
		track_a: coverpoint tr.a
		{
			bins zero = {0};
			bins lo[2] = {[1:2], 3, 4};
			bins hi[] = {[8:$]};
			bins misc = default; // 默认仓不参与覆盖计算
		}
		track_b: coverpoint tr.b;
		cross track_a, track_b // 交叉覆盖
		{
			ignore_bins ignore0 = binsof(track_b) intersect {7}; // 忽略仓
			ignore_bins ignore1 = binsof(track_a) intersect {[9:11]} && binsof(track_b) intersect {0};
			ignore_bins ignore2 = binsof(track_a.lo);
		}
	endgroup

	function new();
		cg = new();
	endfunction

	virtual task main();
		tr = new();
		repeat (num_trans) begin
			assert(tr.randomize());
			tr.print("sequencer");
			cg.sample();
		end
	endtask
endclass

`endif
