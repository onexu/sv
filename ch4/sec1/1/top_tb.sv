`include "dut.sv"
`include "intf.svh"
`include "test.sv"

module top_tb;
	bit clk;
	bit rst_n;
	intf i_intf(clk, rst_n);

	initial begin
		forever #50 clk = !clk;
	end

	initial begin
		rst_n = 1;
		#10 rst_n = 0;
		#20 rst_n = 1;
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	adder i_adder (
		.clk(clk),
		.rst_n(i_intf.rst_n),
		.a(i_intf.a),
		.b(i_intf.b),
		.sum(i_intf.sum));

	test i_test (i_intf);
endmodule
