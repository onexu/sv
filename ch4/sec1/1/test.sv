`ifndef TEST_SV
`define TEST_SV

module automatic test #(parameter WIDTH = 4) (intf i_intf);
	initial begin
		$monitor("@%0t, a=%0d, b=%0d, sum=%0d", $time, i_intf.a, i_intf.b, i_intf.sum);
		// 输出初始值
		i_intf.a <= 4'h0;
		i_intf.b <= 4'h0;
		// 等待复位结束
		@(posedge i_intf.rst_n);
		// 发送激励并采集响应
		repeat(5) begin
			@(posedge i_intf.clk);
			i_intf.a <= $urandom_range(0, 4'hf);
			i_intf.b <= $urandom_range(0, 4'hf);
		end
		$finish();
	end
endmodule

`endif
