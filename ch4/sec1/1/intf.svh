`ifndef INTF_SV
`define INTF_SV

interface intf #(parameter WIDTH = 4) (input clk, input rst_n);
	logic [WIDTH-1:0] a;
	logic [WIDTH-1:0] b;
	logic [WIDTH:0] sum;
endinterface

`endif
