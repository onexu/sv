`ifndef INTF_SV
`define INTF_SV

interface intf #(parameter WIDTH = 4) (input clk, input rst_n);
	logic [WIDTH-1:0] a;
	logic [WIDTH-1:0] b;
	logic [WIDTH:0] sum;

	modport TEST(input clk, rst_n, sum, output a, b,
		import wait_reset_done);

	task wait_reset_done(); // 等待异步复位结束
		@(posedge rst_n);
	endtask
endinterface

`endif
