`ifndef TEST_SV
`define TEST_SV

interface bidir_if #(parameter WIDTH = 4) (input clk);
	wire [WIDTH-1:0] a;
	clocking cb @(posedge clk);
		inout a; // 双向时钟块信号
	endclocking
	modport TEST (clocking cb);
endinterface

module automatic test (bidir_if.TEST i_intf);
	initial begin
		@i_intf.cb;
		i_intf.cb.a <= 'z;      // 三态
		@i_intf.cb;
		$displayh(i_intf.cb.a); // 读数据
		@i_intf.cb;
		i_intf.cb.a <= 4'h5;    // 写数据
		@i_intf.cb;
		$displayh(i_intf.cb.a); // 读数据
		$finish();
	end
endmodule

module top_tb();
	bit clk;
	bidir_if i_intf(clk);

	initial begin
        clk = 1'b0;
		forever #50 clk = !clk;
	end

	test i_test(i_intf);
endmodule

`endif
