`ifndef TEST_SV
`define TEST_SV

module automatic test(intf i_intf);
	initial begin
		$monitor("@%0t, a=%0d, cb_mon.a=%0d", $time, i_intf.a, i_intf.cb_mon.a);
		#40  i_intf.cb_drv.a <= 1; // 在40ns处驱动1
		#110 i_intf.cb_drv.a <= 2; // 在150ns处驱动2
		#120 i_intf.cb_drv.a <= 3; // 在270ns处驱动3
		#200 $finish();
	end
endmodule

`endif
