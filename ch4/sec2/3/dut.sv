`ifndef DUT_SV
`define DUT_SV

module adder #(parameter WIDTH = 4) (
	input logic [WIDTH-1:0] a);

	initial $monitor("@%0t, a=%0d", $time, a);
endmodule

`endif
