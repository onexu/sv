`include "dut.sv"
`include "intf.svh"
`include "test.sv"

module top_tb;
	bit clk;

	initial begin
		clk = 1'b0;
		forever #50 clk = !clk;
	end

	initial begin
		$fsdbDumpfile("./wave.fsdb");
		$fsdbDumpvars("+all");
		$fsdbDumpMDA(0, top_tb);
		$fsdbDumpSVA();
	end

	intf i_intf(clk);

	adder i_adder (i_intf.a);

	test i_test (i_intf);
endmodule
