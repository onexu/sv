`ifndef INTF_SV
`define INTF_SV

interface intf #(parameter WIDTH = 4) (input clk);
	logic [WIDTH-1:0] a;

	clocking cb_drv @(posedge clk); // 用于驱动的时钟块
		default output #10ns; // 指定保持时间
		output a; // 输出时钟块信号
	endclocking

	clocking cb_mon @(posedge clk); // 用于采样的时钟块
		input a; // 输入时钟块信号
	endclocking

	modport TEST(clocking cb_drv, clocking cb_mon, input clk);
endinterface

`endif
