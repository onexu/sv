`ifndef TEST_SV
`define TEST_SV

module automatic test #(parameter WIDTH = 4) (intf.TEST i_intf);
	initial begin
		$monitor("@%0t, a=%0d, b=%0d, sum=%0d", $time, i_intf.cb_mon.a, i_intf.cb_mon.b, i_intf.cb_mon.sum); // 可以使用i_intf.cb_drv.sum
		// 输出初始值
		i_intf.cb_drv.a <= 4'h0; // 不能使用i_intf.cb_mon.a
		i_intf.cb_drv.b <= 4'h0;
		// 等待复位结束
		i_intf.wait_reset_done();
		// 发送激励并采集响应
		repeat(5) begin
			@i_intf.cb_drv; // 可以使用i_intf.cb_mon
			i_intf.cb_drv.a <= $urandom_range(0, 4'hf);
			i_intf.cb_drv.b <= $urandom_range(0, 4'hf);
		end
		$finish();
	end
endmodule

`endif
