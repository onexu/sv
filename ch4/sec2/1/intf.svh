`ifndef INTF_SV
`define INTF_SV

interface intf #(parameter WIDTH = 4) (input clk, input rst_n);
	logic [WIDTH-1:0] a;
	logic [WIDTH-1:0] b;
	logic [WIDTH:0] sum;

	clocking cb_drv @(posedge clk); // 用于驱动的时钟块
		output a, b; // 输出时钟块信号
		input sum; // 输入时钟块信号
	endclocking

	clocking cb_mon @(posedge clk); // 用于采样的时钟块
		input a, b, sum; // 输入时钟块信号
	endclocking

	modport TEST(clocking cb_drv, clocking cb_mon, input clk, rst_n,
		import wait_reset_done);

	task wait_reset_done(); // 等待异步复位结束
		@(posedge rst_n);
	endtask
endinterface

`endif
