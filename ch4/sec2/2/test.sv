`ifndef TEST_SV
`define TEST_SV

module automatic test #(parameter WIDTH = 4) (intf i_intf);
	initial begin
		$monitor("@%0t, sum=%0d, cb_mon0.sum=%0d, cb_mon1.sum=%0d", $time, i_intf.sum, i_intf.cb_mon0.sum, i_intf.cb_mon1.sum);
		// 等待4个时钟周期
		repeat(4) @i_intf.cb_mon0;
		$finish();
	end
endmodule

`endif
