`ifndef INTF_SV
`define INTF_SV

interface intf #(parameter WIDTH = 4) (input clk);
	logic [WIDTH:0] sum;

	clocking cb_mon0 @(posedge clk); // 用于采样的时钟块
		default input #30; // 指定较大的建立时间
		input sum; // 输入时钟块信号
	endclocking

	clocking cb_mon1 @(posedge clk); // 用于采样的时钟块
		default input #10; // 指定较小的建立时间
		input sum; // 输入时钟块信号
	endclocking

	modport TEST(clocking cb_mon0, clocking cb_mon1, input clk);
endinterface

`endif
