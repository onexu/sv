`ifndef DUT_SV
`define DUT_SV

module adder #(parameter WIDTH = 4) (
	output reg [WIDTH:0] sum);

	initial begin
		sum = 0; // 时钟上升沿前20ns输出1
		#30  sum = 1; // 时钟上升沿前20ns输出1
		#100 sum = 2; // 时钟上升沿前20ns输出2
		#20  sum = 3; // 时钟上升沿处输出3
	end
endmodule

`endif
