`ifndef SVM_CONFIG_DB_SVH
`define SVM_CONFIG_DB_SVH

class svm_config_db #(type T = int);
	static T db[string];
	static function void set(input string field_name, input T value);
		db[field_name] = value;
	endfunction

	static function void get(input string field_name, inout T value);
		value = db[field_name];
	endfunction

	static function void dump();
		$display("configuration database %s", $typename(T));
		foreach(db[i])
		$display("db[%s]=%0p", i, db[i]);
	endfunction
endclass

`endif
