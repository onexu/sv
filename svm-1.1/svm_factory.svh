`ifndef SVM_FACTORY_SVH
`define SVM_FACTORY_SVH

virtual class svm_object_wrapper;
	virtual function svm_component create_component(string name); // 创建组件对象
		return null;
	endfunction
	pure virtual function string get_type_name(); // 获取代理对象索引名
endclass

class svm_factory;
	static svm_object_wrapper m_type_names[string]; // 代理对象句柄登记表

	static svm_factory m_inst; // 工厂类（单例类）句柄

	static function svm_factory get(); // 获取工厂对象
		if(m_inst == null) begin // 是否有唯一实例
			m_inst = new();
			$display("%s(%0d) create a unique object of singleton class svm_factory", `__FILE__, `__LINE__);
		end
		return m_inst;
	endfunction

	static function void register(svm_object_wrapper c);
		m_type_names[c.get_type_name()] = c; // 注册代理对象句柄
	endfunction

	static string override [string]; // 代理对象句柄索引覆盖表

	static function void override_type(string type_name, string override_type_name);
		override[type_name] = override_type_name; // 保存代理对象句柄索引覆盖值，索引为原代理对象句柄索引
	endfunction

	function svm_object create_object_by_type(svm_object_wrapper proxy, string name);
		proxy = find_override(proxy); // 查找代理对象句柄
		return proxy.create_component(name);
	endfunction

	function svm_object_wrapper find_override(svm_object_wrapper proxy);
		if(override.exists(proxy.get_type_name)) begin // 判断原始类型名是否注册过
			$display("%s(%0d) class %s is overridden by class %s", `__FILE__, `__LINE__, proxy.get_type_name, m_type_names[override[proxy.get_type_name()]].get_type_name());
			return m_type_names[override[proxy.get_type_name()]]; // 返回覆盖类型名
		end
		return proxy; // 返回原始类型名
	endfunction

	static function svm_component get_test(); // 创建测试对象
		string name;
		svm_object_wrapper test_wrapper;
		svm_component test_comp;

		if(!$value$plusargs("SVM_TESTNAME=%s", name))
			$fatal("+SVM_TESTNAME not found.");
		$display("%s(%0d) running test %s...", `__FILE__, `__LINE__, test_name);
		test_wrapper = svm_factory::m_type_names[name];
		$cast(test_comp, test_wrapper.create_component(name));
		return test_comp;
	endfunction
endclass

`endif
