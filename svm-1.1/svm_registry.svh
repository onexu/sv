`ifndef SVM_REGISTRY_SVH
`define SVM_REGISTRY_SVH

class svm_component_registry #(type T = svm_component,
	string Tname = "<unknown>") extends svm_object_wrapper;
	
	typedef svm_component_registry #(T,Tname) this_type;

	// 创建测试对象
	virtual function svm_component create_component(string name="");
		T obj;
		$display("%s(%0d) create a object of class %s using the create_component method", `__FILE__, `__LINE__, Tname);
		obj = new(name);
		return obj;
	endfunction

	virtual function string get_type_name();
		return Tname;
	endfunction

	// 单例类的自动例化
	local static this_type me = get(); // 静态代理对象句柄

	// 返回代理对象（单例类）句柄
	static function this_type get();
		if (me == null) begin // 是否有唯一实例
			svm_factory f = svm_factory::get(); // 获取工厂类唯一实例
			me = new(); // 创建单例类的唯一实例
			$display("%s(%0d) create a unique object of singleton class svm_component_registry#(%s,%s) using get method", `__FILE__, `__LINE__, Tname, Tname);
			f.register(me); // 在工厂中注册单例类的唯一实例
		end
		return me;
	endfunction

	// 创建组件
	static function T create(string name);
		svm_object obj;
		svm_factory factory = svm_factory::get(); // 获取工厂类唯一实例
		$display("%s(%0d) create a object of class %s using the create method", `__FILE__, `__LINE__, Tname);
		obj = factory.create_object_by_type(me,name); // 创建组件对象
		$cast(create, obj);
	endfunction
endclass

`endif
