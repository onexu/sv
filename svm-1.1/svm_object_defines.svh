`define svm_component_utils(T) \
	typedef svm_component_registry #(T,`"T`") type_id; \
	virtual function string get_type_name (); \
		return `"T`"; \
	endfunction
