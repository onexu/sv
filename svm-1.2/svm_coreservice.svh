`ifndef SVM_CORESERVICE_SVH
`define SVM_CORESERVICE_SVH

typedef class svm_factory;
typedef class svm_root;

class svm_coreservice_t;
	local static svm_coreservice_t inst;
	local svm_factory factory;

	static function svm_coreservice_t get();
		if(inst == null) begin
			inst = new(); // 创建svm_coreservice_t的唯一实例
			$display("%s(%0d) create a unique object of singleton class svm_coreservice_t", `__FILE__, `__LINE__);
		end
		return inst;
	endfunction

	virtual function svm_root get_root();
		return svm_root::m_svm_get_root();
	endfunction

	virtual function svm_factory get_factory();
		if(factory == null) begin
			svm_factory f;
			f = new(); // 创建svm_factory的唯一实例
			$display("%s(%0d) create a unique object of singleton class svm_factory", `__FILE__, `__LINE__);
			factory = f;
		end
		return factory;
	endfunction
endclass

`endif
