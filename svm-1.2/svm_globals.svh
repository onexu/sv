task run_test (string test_name="");
  svm_root top;
  svm_coreservice_t cs;
  cs = svm_coreservice_t::get();
  top = cs.get_root();
  top.run_test(test_name);
endtask
