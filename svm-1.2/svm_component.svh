`ifndef SVM_COMPONENT_SVH
`define SVM_COMPONENT_SVH

virtual class svm_object;
endclass

virtual class svm_component extends svm_object;
	string name;

	function new(string name);
		this.name = name;
	endfunction

	virtual function void build();
	endfunction

	virtual function void connect();
	endfunction

	virtual task main();
	endtask
endclass

`endif
