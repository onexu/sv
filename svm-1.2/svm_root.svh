class svm_root extends svm_component;
	static local svm_root m_inst;

	extern protected function new ();

	extern static function svm_root get();

	virtual function string get_type_name();
		return "svm_root";
	endfunction

	static function svm_root m_svm_get_root();
		if (m_inst == null) begin
			m_inst = new();
			$display("%s(%0d) create a object of class %s using the m_svm_get_root", `__FILE__, `__LINE__, "svm_root");
		end
		return m_inst;
	endfunction

	extern virtual task run_test(string test_name=""); // 创建测试对象
endclass

function svm_root svm_root::get();
	svm_coreservice_t cs = svm_coreservice_t::get();
	return cs.get_root();
endfunction


function svm_root::new();
	super.new("__top__");
endfunction

task svm_root::run_test(string test_name=""); // 创建测试对象
	svm_object_wrapper test_wrapper;
	svm_component svm_test_top;

	if(!$value$plusargs("SVM_TESTNAME=%s", test_name))
		$fatal("+SVM_TESTNAME not found.");
	$display("%s(%0d) running test %s...", `__FILE__, `__LINE__, test_name);

	if (test_name != "") begin
		test_wrapper = svm_factory::m_type_names[test_name];
		$cast(svm_test_top, test_wrapper.create_component(test_name));
	end

	svm_test_top.build();
	svm_test_top.connect();
	svm_test_top.main();
endtask
