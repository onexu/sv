`ifndef SVM_FACTORY_SVH
`define SVM_FACTORY_SVH

virtual class svm_object_wrapper;
	virtual function svm_component create_component(string name); // 创建组件对象
		return null;
	endfunction
	pure virtual function string get_type_name(); // 获取代理对象索引名
endclass

class svm_factory;
	static svm_object_wrapper m_type_names[string];  // 代理对象登记表

	static svm_factory m_inst; // 工厂类（单例类）句柄

	static function svm_factory get(); // 获取工厂对象
		svm_coreservice_t cs;
		cs = svm_coreservice_t::get(); // 获取核心服务类对象
		return cs.get_factory();
	endfunction

	static function void register(svm_object_wrapper c);
		m_type_names[c.get_type_name()] = c; // 注册代理对象句柄
	endfunction

	static string override [string];  // 代理对象句柄索引覆盖表

	static function void override_type(string type_name, string override_type_name);
		override[type_name] = override_type_name;  // 保存代理对象句柄索引覆盖值，索引为原代理对象句柄索引
	endfunction

	function svm_object create_object_by_type(svm_object_wrapper proxy, string name);
		proxy = find_override(proxy); // 返回代理对象句柄
		return proxy.create_component(name);
	endfunction

	function svm_object_wrapper find_override(svm_object_wrapper proxy);
		if(override.exists(proxy.get_type_name)) begin  // 判断原始类型名是否注册过
			$display("%s(%0d) class %s is overridden by class %s", `__FILE__, `__LINE__, proxy.get_type_name, m_type_names[override[proxy.get_type_name()]].get_type_name());
			return m_type_names[override[proxy.get_type_name()]];  // 返回覆盖类型名
		end
		return proxy; // 返回原始类型名
	endfunction
endclass

`endif
