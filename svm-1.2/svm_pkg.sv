`ifndef SVM_PKG_SV
`define SVM_PKG_SV

package svm_pkg;
	`include "svm_config_db.svh"
	`include "svm_object_defines.svh"
	`include "svm_component.svh"
	`include "svm_coreservice.svh"
	`include "svm_factory.svh"
	`include "svm_registry.svh"
	`include "svm_globals.svh"
    `include "svm_root.svh"
endpackage

`endif
