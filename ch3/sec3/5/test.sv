`ifndef TEST_SV
`define TEST_SV

module automatic test;
	function int init (input int din);
		static int t;
		t = din;
		init = t;
	endfunction
	
	initial $display("%0d", init(1));
endmodule

`endif
