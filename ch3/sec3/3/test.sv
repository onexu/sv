`ifndef TEST_SV
`define TEST_SV

module automatic test;
	logic [3:0] a;

	task init();
		#5 a = 1;
		$display("in task, a=%0d", a);
		#5 a = 2;
		$display("in task, a=%0d", a);
	endtask

	initial begin
		$monitor("in test, a=%0d", a);
		a = 0;
		init();
		#20 $finish();
	end
endmodule

`endif
