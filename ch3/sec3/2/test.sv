`ifndef TEST_SV
`define TEST_SV

task automatic init(output bit [3:0] a); // 修改为ref，实参会同时变化
	#5 a = 1;
	$display("in task, a=%0d", a);
	#5 a = 2;
	$display("in task, a=%0d", a);
endtask

module automatic test;
	bit [3:0] a;

	initial begin
		$monitor("in test, a=%0d", a);
		a = 0;
		init(a);
		#20 $finish();
	end
endmodule

`endif
