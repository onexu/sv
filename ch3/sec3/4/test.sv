`ifndef TEST_SV
`define TEST_SV

module automatic test;
	byte a[] = '{-1, -2, 3, 4};

	function int sum(input int unsigned lo = 0, input int unsigned hi = 0);
		if (hi > (a.size() - 1))
			return 0;
		sum = 0;
		for (int i = lo; i <= hi; i++)
			sum += a[i];
	endfunction

	initial begin
		$display("sum=%0d", sum());
		$display("sum=%0d", sum(1, 4));
		$display("sum=%0d", sum(2));
		$display("sum=%0d", sum(, 3));
	end
endmodule	

`endif
