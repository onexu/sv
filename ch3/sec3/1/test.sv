`ifndef TEST_SV
`define TEST_SV

module automatic test();
	int file, data[$];
	initial begin
		file = $fopen("../data.txt", "r");
		for(int i = 0; i < 16; i++)
			void'($fscanf(file, "%h", data[i]));
		$display("%p", data);
	end
endmodule

`endif
