`ifndef TEST_SV
`define TEST_SV

module automatic test;
	function bit [31:0] factorial (input bit [31:0] a);
		if (a >= 2)
			factorial = factorial(a - 1) * a;
		else
			factorial = 1;
	endfunction

	initial begin
		for (int i = 0; i < 4; i++)
			$display("%0d!=%0d", i, factorial(i));
	end
endmodule

`endif
