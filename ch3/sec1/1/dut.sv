`ifndef DUT_SV
`define DUT_SV

module dut (
	input [3:0] a, b,
	output [3:0] c,
	output reg [3:0] d, e, f);
	
	assign c = a + b;
	always_comb d = a + b;
	always_latch if (a == 0) e = a + b;
	always_comb f = a + b;
endmodule

`endif