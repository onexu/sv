`include "dut.sv"
`include "test.sv"

module top_tb;
	bit clk;
	logic [3:0] a, b, c, d, e, f;

	initial begin
        clk = 0;
        forever #50 clk = ~clk;
    end

	test i_test(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c),
		.d(d),
		.e(e),
		.f(f));

	dut i_dut(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c),
		.d(d),
		.e(e),
		.f(f));
endmodule