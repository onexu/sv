`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input logic clk,
	input logic [3:0] c, d, e, f,
	output logic [3:0] a, b);

	initial $monitor("@%0t, a=%0d, b=%0d, c=%0d, d=%0d, e=%0d, f=%0d", $time, a, b, c, d, e, f);

	initial begin
		a <= 0;
		b <= 0;
		repeat(2) begin
			@(posedge clk) a <= $urandom_range(0,7);
			@(posedge clk) b <= $urandom_range(0,8);
		end
		repeat(2) @(posedge clk);
		$finish();
	end
endmodule

`endif
