`ifndef GUARD_DUT_SV
`define GUARD_DUT_SV

module dut (
	input clk,
	input [3:0] a,
	output reg [3:0] b, c);
	
	// initial c = 'h0; // 编译错误

	always_ff @(posedge clk) begin // 过程赋值，非阻塞赋值
		b <= a;
		c <= b + 1;
	end
endmodule

`endif
