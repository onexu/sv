`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input clk,
	input [3:0] b, c,
	output reg [3:0] a);

	initial begin
		$monitor("@%0t, a=%0d, b=%0d, c=%0d", $time, a, b, c);
		repeat(3) begin
			@(posedge clk);
			a <= $urandom_range(0,7);
		end
		@(posedge clk);
		$finish();
	end
endmodule

`endif
