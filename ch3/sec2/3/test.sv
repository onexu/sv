`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		repeat(8) begin
			case ($urandom_range(100)) inside
				[90:100]: $display("A");
				[70: 89]: $display("B");
				[60: 69]: $display("C");
				default:  $display("F");
			endcase
		end
	end
endmodule

`endif

