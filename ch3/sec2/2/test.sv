`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		for(int i = 0; i < 10; i++) begin
			if(i < 4) continue;
			if(i > 7) break;
			$display("i=%0d", i);
		end
	end
endmodule

`endif
