`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin : example // 使用标记符
		int sum;
		for (int i=0; i<10; i++) begin // 定义局部循环变量
			sum += i; // 自加运算符
		end
		$display("sum=%0d", sum);
	end : example // 使用标记符
endmodule : test // 模块名作为标记符

`endif
