`ifndef TEST_SV
`define TEST_SV

package pkg_2d;
	typedef struct {int x, y;} point2_s;
	string message = "message in pkg_2d";
endpackage

package pkg_3d;
	typedef struct {int x, y, z;} point3_s;
	string message = "message in pkg_3d";
endpackage

module automatic test;
	import pkg_2d::*;
	import pkg_3d::point3_s;

	point2_s  xy;
	point3_s xyz;
	string message = "message in test"; // 使pkg_2d包中的message不可见
	
	initial begin
		$display("%s, %s", message, pkg_2d::message);
	end
endmodule

`endif
