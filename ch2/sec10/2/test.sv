`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef union {bit [7:0] bu; byte bs;} num_u;
	initial begin
		num_u uni;
		uni.bu = 255;
		$display("uni.bu=%0d, uni.bs=%0d", uni.bu, uni.bs);
	end
endmodule

`endif
