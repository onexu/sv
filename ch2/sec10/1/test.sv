`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef struct {bit [7:0] r, g, b;} pixel_s;
	typedef struct packed {bit [7:0] r, g, b;} packed_pixel_s;
	initial begin
		pixel_s pixel = '{8'h1, 8'h2, 8'h3};
		packed_pixel_s packed_pixel = {8'h1, 8'h2, 8'h3};
		$display("pixel=%p", pixel);
		$display("packed_pixel=%p", packed_pixel);
	end
endmodule

`endif
