`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[$] = {0,2,3}, b = 1;
		a.insert(1, b); // 插入变量b
		a.insert(2, 4); // 插入常量4
		$display("a=%p", a);
		a.delete(2);
		$display("a=%p", a);
		a.push_front(-1);
		$display("a=%p", a);
		a.push_back(4);
		$display("a=%p", a);
		a.pop_front;
		$display("a=%p", a);
		b = a.pop_back;
		$display("a=%p, b=%0d", a, b);
		a.delete(); // 删除队列
	end
endmodule

`endif
