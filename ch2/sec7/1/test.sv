`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[$] = {0,2,3};
		$display("a=%p", a); // 访问整个队列
		$display("a[0]=%0d", a[0]); // 访问队列第一个元素
		$display("a[$]=%0d", a[$]); // 访问队列最后一个元素
		$display("a[$-1]=%0d", a[$-1]); // 访问队列倒数第二个元素
	end
endmodule

`endif
