`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[$] = {0, 2, 3}, b = 1;
		a = {a[0], b, 4, a[1:$]};
		$display("a=%p", a);
		a = {a[0:1], a[3:$]};
		$display("a=%p", a);
		a = {-1, a}; // push_front
		$display("a=%p", a);
		a = {a, 4}; // push_back
		$display("a=%p", a);
		a = a[1:$]; // pop_front
		$display("a=%p", a);
		b = a[$];
		a = a[0:$-1]; // pop_back
		$display("a=%p, b=%0d", a, b);
		a = {}; // delete
	end
endmodule

`endif
