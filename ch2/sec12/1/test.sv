`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		shortint h;
		bit [3:0] a[4] = '{'ha, 'hb, 'hc, 'hd};
		bit [3:0] b[4], c, d, e, f;
		h = {>>{a}}; // 将数组a打包到h
		$display("%h", h);
		h = {<<{a}}; // 将数组a按位逆序打包到h
		$display("%h", h);
		h = {<<byte{a}}; // 将数组a按字节逆序打包到h
		$display("%h", h);
		{>>{b}} = {<<4{a}}; // 将数组a按半字节逆序并解包到数组b
		$display("%p", b);
		{>>{c, d, e, f}} = a; // 将数组a解包成多个半字节
		h = {>>{f, e, d, c}}; // 将多个半字节打包到h
		$display("%h", h);
	end
endmodule

`endif
