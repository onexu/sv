`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [9:0] a[$] = '{10'h1, 10'h2, 10'h3};
		bit [7:0] b[];
		b = {>>bit[7:0]{a}};
		foreach(a[i])
			$write("%b", a[i]);
		$display("");
		foreach(b[i])
			$write("%b", b[i]);
		$display("\n%p", b);
	end
endmodule

`endif
