`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef struct {
		int unsigned a;
		byte unsigned b;
	} my_struct_s;

	initial begin
		my_struct_s st = '{32'h12345678, 8'h9a};
		byte unsigned b[];
		b = {>> {st}}; // 将结构转换成字节数组
		$display("b=%p", b);
		st = {>> {b}}; // 将字节数组转换成结构
		$display("st=%p", st);
	end
endmodule

`endif
