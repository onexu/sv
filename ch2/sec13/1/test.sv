`ifndef TEST_SV
`define TEST_SV

`define SUM(a, b) ((a)+(b))

module automatic test;
	initial begin
		$display("%0d", `SUM(1+2, 2+3) * `SUM(1+2, 2+3));
	end
endmodule

`endif
