`ifndef TEST_SV // 如果没有定义宏`TEST_SV
`define TEST_SV // 定义宏`TEST_SV

`define RTL // 定义宏`RTL

module automatic test;
	initial begin
		bit a, b;
		b = 1;
`ifdef RTL // 如果定义宏`RTL
		a = ~b;
`elsif GATE // 如果没有定义宏`RTL，且定义宏`GATE
		not not_u1 (a, b);
`else // 否则
		a = b ? 0 : 1;
`endif // 结束编译指令，与`ifdef对应
		$display("%0d", a);
	end
endmodule

`endif // 结束编译指令，与`ifndef对应
