`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef enum {IDLE, INIT, DECODE} state_e;
	initial begin
		state_e state;
		int s;
		state = INIT;
		s = state;
		if (!$cast(state, ++s)) // 动态转换
			$display("Cast failed for s=%0d", s);
		$display("State %s=%0d", state.name, state);
		state = state_e'(++s); // 静态转换
		$display("State %s=%0d", state.name, state);
	end
endmodule

`endif
