`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef enum {IDLE, INIT, DECODE} state_e;
	initial begin
		state_e state;
		state = state.first;
		do begin
			$display("State %s=%0d", state.name(), state);
			state = state.next;
		end while (state != state.first);
	end
endmodule

`endif
