`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef enum {IDLE, INIT, DECODE} state_e;
	initial begin
		state_e pstate, nstate;
		case (pstate)
			IDLE: nstate = INIT;
			INIT: nstate = DECODE;
			default: nstate = IDLE;
		endcase
		$display("next state %s=%0d", nstate.name(), nstate);
	end
endmodule

`endif
