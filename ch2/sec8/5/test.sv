`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int d[] = '{3,1,4,1,5,9};
		d.reverse();
		$display("d=%p", d);
		d.sort();
		$display("d=%p", d);
		d.rsort();
		$display("d=%p", d);
		d.shuffle();
		$display("d=%p", d);
	end
endmodule

`endif
