`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int f[6] ='{1,6,2,6,8,6};
		int d[] ='{2,4,6,8};
		int q[$]= {1,3,5,7};
		int tq[$];  // 保存结果的临时队列
		tq = q.min(); // 操作队列
		$display("%p", tq);
		tq = d.max(); // 操作动态数组
		$display("%p", tq);
		tq = f.unique(); // 操作定长数组
		$display("%p", tq);
		tq = f.unique_index();
		$display("%p", tq);
	end
endmodule

`endif
