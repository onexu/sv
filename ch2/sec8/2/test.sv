`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[int] = '{0:0, 10:2, 100:4, 1000:6, 10000:8};
		int r, count = 0;
		r = $urandom_range(a.size()-1);
		foreach(a[i]) begin
			if (count++ == r) begin
				$display("The %0d'th element a[%0d]=%0d", r, i, a[i]);
				break; // 跳出循环
			end
		end
	end
endmodule

`endif
