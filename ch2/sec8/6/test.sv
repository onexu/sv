`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		typedef struct { bit [7:0] r, g, b; } color_s;
		color_s color[] = '{'{r:1, g:4, b:7}, '{r:2, g:5, b:8}, '{r:1, g:5, b:9}};
		color.sort with (item.g); // 只排序g
		$display("color=%p", color);
		color.sort with ({item.g, item.b}); // 先排序g，在排序b
		$display("color=%p", color);
	end
endmodule

`endif
