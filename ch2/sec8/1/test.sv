`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[] = '{1, 2, 3, 4};
		$write("%0d ", a.sum());
		$write("%0d ", a.sum with (int'(item > 2)));
		$write("%0d ", a.sum(x) with (x + 1));
		$write("%0d ", a.product);
		$display("%0d", a.and);
	end
endmodule

`endif
