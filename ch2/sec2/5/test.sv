`ifndef TEST_SV
`define TEST_SV

module automatic test;
	byte a, b, sum;
	logic c;
	initial begin
		a = 8'd127;
		b = 8'd1;
		sum = a + b;
		$display("a=%0d, b=%0d, sum=%0d", a, b, sum);
		b = c;
		$display("c=%0d, b=%0d", c, b);
	end
endmodule

`endif

