`ifndef TEST_SV
`define TEST_SV

module automatic test #(parameter WIDTH = 4) (
	input bit clk,
	input bit rst_n,
	input bit [WIDTH:0] sum,
	output bit [WIDTH-1:0] a,
	output bit [WIDTH-1:0] b);

	initial begin
		// 输出初始值
		a <= 4'h0;
		b <= 4'h0;
		// 等待复位结束
		@(posedge rst_n);
		// 发送激励并采集响应
		repeat(5) begin
			@(posedge clk);
			a <= $urandom_range(0, 4'hf);
			b <= $urandom_range(0, 4'hf);
			@(posedge clk);
			$display("a=%0d, b=%0d, sum=%0d", a, b, sum);
		end
		$finish();
	end
	final $display("end simulation");
endmodule

`endif
