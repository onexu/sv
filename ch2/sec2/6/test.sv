`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [7:0] a = 8'haa; // 最高位为a[7]，最低位为a[0]
		bit [0:7] b = 8'haa; // 最高位为b[0]，最低位为b[7]
		$display("%b, %b, %b", a, a[0], a[2:0]); // a[0:2]非法范围
		$display("%b, %b, %b, %b", a[1+:5], a[5:1], a[4-:3], a[4:2]);
		$display("%b, %b, %b", b, b[0], b[0:2]); // b[2:0]非法范围
		$display("%b, %b, %b, %b", b[1+:5], b[1:5], b[4-:3], b[2:4]);
	end
endmodule

`endif
