`include "uvm_macros.svh"
import uvm_pkg::*;

`include "dut.sv"
`include "test.sv"

module top_tb;
	wire [3:0] a, b, c, d, e, f;

	test i_test(
		.a(a),
		.b(b),
		.c(c),
		.d(d),
		.e(e),
		.f(f));

	dut i_dut(
		.a(a),
		.b(b),
		.c(c),
		.d(d),
		.e(e),
		.f(f));
endmodule
