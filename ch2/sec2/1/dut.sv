`ifndef DUT_SV
`define DUT_SV

module dut (
	input [3:0] a, b,
	output [3:0] c,
	output reg [3:0] d, e, f);
	
	assign c = a + b; // 连续赋值
	always @(*) d = a + b; // 过程赋值，阻塞赋值
	always @(*) if (a == 0) e = a + b; // 过程赋值，阻塞赋值，缺少else语句，锁存器
	always @(b) f = a + b; // 过程赋值，阻塞赋值，敏感列表不完整，锁存器
endmodule

`endif
