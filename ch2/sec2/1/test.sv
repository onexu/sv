`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input [3:0] c, d, e, f,
	output reg [3:0] a, b);

	initial begin
		$monitor("@%0t, a=%0d, b=%0d, c=%0d, d=%0d, e=%0d, f=%0d", $time, a, b, c, d, e, f);
		#10;
		a = 0;
		b = 0;
		#10;
		a = 1;
		b = 1;
		#10;
		a = 2;
		#10;
		$finish();
	end
endmodule

`endif
