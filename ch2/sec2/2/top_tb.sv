`include "uvm_macros.svh"
import uvm_pkg::*;

`include "dut.sv"
`include "test.sv"

module top_tb;
	reg clk;
	wire [3:0] a, b, c;

	initial begin
		clk = 1'b0;
		forever #50 clk = ~clk;
	end

	test i_test(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c));

	dut i_dut(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c));
endmodule
