`ifndef GUARD_DUT_SV
`define GUARD_DUT_SV

module dut (
	input clk,
	input [3:0] a,
	output reg [3:0] b, c);
	
	always @(posedge clk) begin // 过程赋值，非阻塞赋值
		b <= a; // 2条语句交换顺序不影响运行结果
		c <= b + 1;
	end
endmodule

`endif
