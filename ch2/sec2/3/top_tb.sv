`include "dut.sv"
`include "test.sv"

module top_tb;
	reg clk;
	wire [3:0] a, b, c, d;

	initial begin
		clk = 1'b0;
		forever #50 clk = ~clk;
	end

	test i_test(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c),
		.d(d));

	dut i_dut(
		.clk(clk),
		.a(a),
		.b(b),
		.c(c),
		.d(d));
endmodule
