`ifndef GUARD_DUT_SV
`define GUARD_DUT_SV

module dut (
	input clk,
	input [3:0] a,
	output reg [3:0] b, c, d);
	
	always @(posedge clk) begin // 过程赋值，阻塞赋值
		b = a;
		c = b + 1;
	end
	always @(posedge clk) begin // 过程赋值，阻塞赋值
		d = b + 1;
	end
endmodule

`endif
