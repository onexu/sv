`ifndef TEST_SV
`define TEST_SV

module automatic test(
	input clk,
	input [3:0] b, c, d,
	output reg [3:0] a);

	initial begin
		repeat(4) begin
			@(posedge clk);
			a <= $urandom_range(0,7);
			$display("a=%0d, b=%0d, c=%0d, d=%0d", a, b, c, d);
		end
		$finish();
	end
endmodule

`endif
