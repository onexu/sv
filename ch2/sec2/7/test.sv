`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		string s;
		s = "Abcd";
		$display("%0d", s.getc(0));
		$display(s.tolower());
		$display(s.toupper());
		$display(s.substr(1, 2));
		s.putc(s.len()-1, " ");
		s = {s, "EF"};
		$display(s);
	end
endmodule

`endif
