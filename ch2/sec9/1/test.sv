`ifndef TEST_SV
`define TEST_SV

module automatic test;
	typedef int array5_t[5];
	initial begin
		array5_t a; // int a[5]
		foreach (a[i])
			a[i] = i;
	end
endmodule

`endif
