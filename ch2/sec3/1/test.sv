`ifndef TEST_SV
`define TEST_SV

`include "ps.sv"
`include "ns.sv"

module automatic test;
	initial begin
		$timeformat(-9, 3, "ns", 8);
		#1     $display("%0t", $realtime);
		#2ns   $display("%0t", $realtime);
		#0.1ns $display("%0t", $realtime);
		#41ps  $display("%0t", $realtime);
	end
endmodule

`endif
