`ifndef TEST_SV
`define TEST_SV

module automatic test;
	timeunit 1ns; // 时间单位为1ns
	timeprecision 100ps; // 时间精度为100ps
	initial begin
		realtime rtdelay = 800ps; // 保存为800ps
		time tdelay = 800ps;      // 四舍五入到1ns
		$timeformat(-12, 0, "ps", 5);
		#rtdelay $display("%0t", rtdelay);
		#tdelay $display("%0t", tdelay);
	end
endmodule

`endif