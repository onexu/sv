`ifndef TEST_SV
`define TEST_SV

module automatic test;
	timeunit 1ns;
	timeprecision 1ps;
	initial begin
		$timeformat(-9, 3, "ns", 8);
		#1     $display("%0t", $realtime);
		#2ns   $display("%0t", $realtime);
		#0.1ns $display("%0t", $realtime);
		#41ps  $display("%0t", $realtime);
	end
endmodule

`endif
