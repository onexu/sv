`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int d1[], d2[], d3[5];
		d1 = new[5]; // 动态数组申请5个元素
		foreach (d1[i])
			d1[i] = i; // 元素初始化
		d2 = d1; // 动态数组赋值给动态数组
		d3 = d1; // 动态数组赋值给定长数组（同长度）
		d1 = new[10](d1); // 数组申请10个元素，再将原有内容复制进数组
		$display("%p", d1);
		d2 = d1;
		// d3 = d1; // 错误，动态数组赋值给定长数组（不同长度）
		d1.delete(); // 删除数组全部元素
	end
endmodule

`endif
