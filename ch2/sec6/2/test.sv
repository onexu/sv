`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int range[string] = '{"min":0, "max":256};
		foreach(range[i])
			$display("%0d", range[i]);
		if (range.exists("max"))
			range["max"] = 1024;
		$display("%p", range);
		$display("%0d", range["mid"]); // 输出默认值0
	end
endmodule

`endif
