`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[int], i;
		a = '{0:0, 10:2, 100:4, 1000:6, 10000:8};
		foreach (a[i])
			$display("a[%0d]=%0d", i, a[i]);
		if (a.first(i)) begin // 得到第一个元素的索引
			do
				$display("a[%0d]=%0d", i, a[i]);
			while (a.next(i)); // 得到第二个元素的索引
		end
		if (a.first(i))
			a.delete(i);
		$display("The array now has %0d elements", a.num);
	end
endmodule

`endif
