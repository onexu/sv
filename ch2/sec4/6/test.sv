`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [7:0] a = 8'b11110000;
		for (int i = 0; i < 8; i++) // 遍历从最低位开始
			$write("%0b", a[i]);
		$display;
		foreach (a[i]) // 遍历从最高位开始
			$write("%0b", a[i]);
		$display;
	end
endmodule

`endif
