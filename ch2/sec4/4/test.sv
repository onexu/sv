`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [3:0] a[4];
		for (int i=0; i<$size(a); i++) // 定义内部循环变量
			a[i] = i;
		foreach (a[i])
			$display("a[%0d]=%0d", i, a[i]);
		$displayb(a[0],, a[0][0],, a[0][2:1]);
	end
endmodule

`endif
