`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit[3:0] [7:0] a[2]; // 包含2个数组元素，每个元素使用二维压缩数组描述
		bit[7:0] [3:0] b; // 包含8个半字节（nibble）的压缩数组
		a[0] = 32'hcafe_dada;
		a[0][3] = 8'h1;
		a[0][1][7] = 1'b1;
		a[0][0][7:4] = 4'b1011;
		b = a[1]; // 压缩数组元素复制
		if(b == a[1]) // 数组的比较
			$displayb(a[0],, a[0][3],, a[0][1][7],, a[0][0][7:4]);
	end
endmodule

`endif
