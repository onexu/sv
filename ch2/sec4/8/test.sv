`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [3:0] a[4] = '{0, 1, 2, 3}; // bit [3:0] a[0:3]
		bit [3:0] b[3:0] = '{0, 1, 2, 3};
		bit [3:0] [3:0] c = '{0, 1, 2, 3};
		$displayh(a,, a[0],, a[1],, a[2],, a[3]);
		$displayh(a[1+:2],, a[1:2],, a[3-:2],, a[2:3]); // a[2:1]非法范围
		$displayh(b,, b[0],, b[1],, b[2],, b[3]);
		$displayh(b[1+:2],, b[2:1],, b[3-:2],, b[3:2]); // b[1:2]非法范围
		$displayh(c,, c[0],, c[1],, c[2],, c[3]);
		$displayh(c[1+:2],, c[2:1],, c[3-:2],, c[3:2]); // c[1:2]非法范围
	end
endmodule

`endif
