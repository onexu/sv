`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [31:0] a[5] = '{0,1,2,3,4}, b[5] = '{5,4,3,2,1};
		if (a == b) // 数组的比较
			$display("a is equal to b");
		else
			$display("a is not equal to b");
		b = a; // 数组的赋值
		$display("a[1:4]%sb[1:4]", (a[1:4] == b[1:4]) ? "==" : "!=");
	end
endmodule

`endif
