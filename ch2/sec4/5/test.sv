`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[2][2] = '{'{0,1}, '{2{2}}};
		foreach(a[i,j])
			$display("a[%0d][%0d]=%0d", i, j, a[i][j]);
		foreach (a[i]) begin // 第一维度
			$write("%0d:", i);
			foreach(a[,j]) // 第二维度
				$write("%2d", a[i][j]);
			$display;
		end
	end
endmodule

`endif
