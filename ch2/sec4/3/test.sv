`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		int a[2][3] = '{'{0,1,2},'{3{4}}};
		int b[2][4] = '{2{'{2{4, 5}}}};
		int c[2] = '{0,1};
		int d[3] = '{default:2};
		$display("a=%p", a);
		$display("b=%p", b);
		$display("c=%p", c);
		$display("d=%p", d);
		d = {{8{4'hf}}, c};
		$display("d=%p", d);
	end
endmodule

`endif
