`ifndef TEST_SV
`define TEST_SV

module automatic test;
	initial begin
		bit [31:0] a = 32'h89abcdef;
		bit [3:0] [7:0] b;
		$displayh(a,, a[3:0],, a[0]);
		b = a;
		$displayh(b,, b[3:2],, b[3],, b[1][3:0],, b[0][4]);
		if(a == b)
			$display("a equals b");
	end
endmodule

`endif
